## Present
Zhihao
Edgar
Diogo
Michel
Christian
Xiang
Adam
Kelvin
Slava
Konstantin
Adriaan (joined later)



## Highlights

_None_

  
  

## Breaking changes

_None_

  
  

## Quantify-scheduler

- Feature added to Zhinst backend which allows an AWG to orchestrate other AWGs or UHFQAs. (https://gitlab.com/quantify-os/quantify-scheduler/-/tree/264-marker-and-triggers-not-configured-properly-in-zi-backend)

- New compilation backend has unwanted clocks bug for qblox backend. (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/278, https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/371). Fix testing on actual hardware still in progress.

  

#### Merged MRs (highlights)

- New TransmonElement using QCoDeS submodules (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/374).

  
  

## Quantify-core

- Saving user-defined text files inside experiment container (https://gitlab.com/quantify-os/quantify-core/-/issues/302)

- Memory leakage due to matplotlib saved figures. (https://gitlab.com/quantify-os/quantify-core/-/issues/298)

  

#### Merged MRs (highlights)

_None_

  
  

## Requested discussions / Any other business

- Nested MeasurementControl, and formalizing relationships between dataset (https://gitlab.com/quantify-os/quantify-core/-/issues/303)
	- Explanation by Diogo
	- Christian: potential solution to use explicit loop and link datasets to initial resonator measurement. He will send a notebook to Diogo.
	- Adam: done in OQS-internal repository by including gettables in other gettables. Two plotmons, one for the inner, one for the outer measurement. An example can be provided.
	- Kelvin: Hierarchical relations between the dataset of outer and inner measurements will be established in version 2 of the dataset.
	- Slava: can make TUIDs gettables


- Mixer calibration #287 (Christian)
	- Hacky solution: don't recompile and upload waveforms directly. Problem: need to guess time it takes to upload.
	- Michel: backend packages need to be updated.
	- To be investigated further once the new waveform correction layer has been formalized


- Multiple zhinst versions #288 (Zhihao)
	- Zhihao to provide the firmware that he tests his code on


- Miscellaneous
	- Edgar: qcodes pinned because of readthedocs failures. Light at the end of the tunnel: *partial* failures with sphinx4 and qcodes 0.33.0
	- Kelvin: zhinst opened up older version of documentation
	- Adriaan: zhinst compatibility was (and should be) brought up from different sides




## Pending topics (for next DMs)
