## Present:

### Maintainers and Project Owners

- [X] Kelvin
- [X] Damien
- [X] Slava
- [ ] Adriaan (PO)
- [ ] Jules (PO)

### Community

- [ ] Jordy
- [ ] Adam
- [X] Diogo
- [ ] Gijs
- [X] Damaz
- [ ] Vraj
- [ ] Tobias
- [ ] Achmed

## Introduction new member:

- Martin

## Highlights:

## Breaking changes:
- No breaking changes!

### Quantify-scheduler:
- Present milestones. (https://gitlab.com/quantify-os/quantify-scheduler/-/milestones)
- Pydantic: Too strict, since it forces the user to set the values of the parameters.
- Present schedulable test result on ZI backend. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/309)
  - Still some issues that need to be ironed out, limited by hardware availability
- I/Q convention
  - Make an issue meant to discuss the best way to handle this.
  - Proposed ideas:
    - Allow the user to specify the IQ convention in the hw config file (I leads Q/Q leads I/counterclockwise/clockwise)
  - Setting it in the HW config will only fix the frequency shift issue, but we will still be dealing with the complex conjugate during the gettable. 
    - Proposed solution: Implement a Readout abstraction layer?

#### Merged MRs (highlights)
- No changes
  
### Quantify-core:
- Please mind we set the deadline for a 1.0 release before the 14th of March.
  - Milestones: https://gitlab.com/quantify-os/quantify-core/-/milestones/9

- Analysis: 
  - Doesn't work with dataset V2 yet
  - Still needs refactoring to change how it deals with the data backend.


#### Merged MRs (highlights):
- No changes

## Requested discussions:
- Present Sprint strategy for quantify-core. (https://hackmd.io/@slavoutich/BkqOxmKAF)
  - All still welcomed to join.

## AOB:
- Edgar cannot join the meeting on Fridays. Request to change the meeting to another weekday. 
