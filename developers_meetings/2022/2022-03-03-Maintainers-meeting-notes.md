## Present:

### Maintainers and Project Owners

- [x] Kelvin
- [x] Damien
- [x] Slava
- [ ] Adriaan (PO)
- [ ] Jules (PO)

### Community

- [ ] Jordy
- [x] Adam
- [x] Diogo
- [ ] Gijs
- [ ] Damaz
- [ ] Vraj
- [x] Tobias
- [x] Achmed
- [x] Edgar
- [x] Michiel
- [x] Konstantin
- [x] Xiang
- [x] Zhihao

## Introduction new member:
- Zhihao is a PhD candidate doing experiments with Zurich instrument hardware. Plans to work on scheduler to include classical instructions. Interface with new quantum programming language "quingo" ([website]([https://gitee.com/quingo](https://gitee.com/quingo)), [docs]([https://quingo.gitee.io/docs/](https://quingo.gitee.io/docs/))).

## Highlights:
- Docs issue for quantify-scheduler (zhinst). Maintainers decided to remove intersphinx mapping of zhinst related objects.
- Bugfixes in qblox backend.

## Breaking changes:
- * Instrument Coordinator - IC stop function has an `allow_failure` parameter which allows IC components attached to it to fail to stop with warning instead of raising errors. Allows for situations when some components cannot have a stop instruction sent before the prepare stage. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/359)
- * Zhinst backend - Removed `latency` and `line_trigger_delay` keys in the channels of the devices for the Zhinst hardware config. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/363)


### Quantify-scheduler:
- [Present milestones](https://gitlab.com/quantify-os/quantify-scheduler/-/milestones)
- Scheduled to release new version on Friday (March 4th, 2022) including Zhinst hardware bugfix.
- Decided to postpone release to Monday, March 7th to allow Edgar to verify that documentation works.
- Discussion whether this release should be `0.5.X` or `0.6.0`. Decided for `0.6.0`, because new features (updates to transmon backend) are included.
- **todo**: change milestone for unmerged features to `0.7`

#### Merged MRs (highlights)
- Adds multiplexing schedule to the verification schedules (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/329).


### Quantify-core:
- There will be delays for the release of v1.0 due to many intersecting circumstances.
- Everybody, who could be dependent on this, is aware
- Plan was ambitious to begin with. To be discussed further in maintainers' meeting.


#### Merged MRs (highlights):


## Requested discussions:

## AOB:
Tobias: open-pipe issue of qblox drivers --> outside the scope of this meeting. Schedule a private meeting with Jordy.

Michiel: Background colour of images reported by Adam
Options: transparent/white/black background? Or configurable? Just generate two/three versions.
Opinions: everybody wants their own customization anyway; white by default, allow option to make transparent; keep analysis interface easy for user
--> Start/continue discussion on slack or in separate issue

Tobias: more info on two-day session on quantify-scheduler? --> planned for after March meeting; no more information at this point

## Pending topics (for next MMs):

- No pending topics
