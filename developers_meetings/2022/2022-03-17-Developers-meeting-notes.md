
_FYI: condensed meeting template in agreement with Kelvin_ 

## Highlights
- Quantify-scheduler 0.6.0 released! (https://gitlab.com/quantify-os/quantify-scheduler/-/releases/0.6.0)


## Breaking changes
_None_


## Quantify-scheduler
- New compilation backend has unwanted clocks bug for qblox backend. (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/278, https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/371). Fix testing on actual hardware still in progress.

#### Merged MRs (highlights)
_None_


## Quantify-core
- Potential bug in Ramsey analysis class. (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/316)
- Pyqt warning when running experiments issue resolved. (https://gitlab.com/quantify-os/quantify-core/-/issues/289)

#### Merged MRs (highlights)
- Resolved the annoying warnings from pyqt. https://gitlab.com/quantify-os/quantify-core/-/merge_requests/314


##  Requested discussions / Any other business
- _Edgar (not mentioned in the meeting itself): Should we start calling these Developer Meetings instead_? (Not discussed)
- Zhihao commented that maybe we should not be in alpha stage anymore.
    - Kelvin noted that we're still in alpha due to lack of documentation.
- Tobias asked who would be assigned to complete the clocks bug in the qblox backend.
    - No one yet, and to be discussed amongst the maintainers.
- Pieter wanted to know when dataset v2 for quantify-core would be available.
    - Practical due date would be mid-April as a lot of contributors are still busy until the end of March.

## Pending topics (for next MMs)
