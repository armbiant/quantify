## Announcement
- Please take note that due to the recent gitlab subscription fiasco external contributors who were given access to the project will need to be removed temporarily until we can find a solution together with gitlab to recognize us as officially an open source project. You will then be re-added again to the project.

## Highlights
- Quantify is now using OSI approved BSD-3 clause license. (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/356, https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/429)

## Breaking changes
_None_

## Quantify-scheduler
_None_

#### Merged MRs (highlights)
- Qblox backend - distortion corrections parameters can now be numpy arrays (quantify-scheduler!426)

## Quantify-core
_None_

#### Merged MRs (highlights)
_None_


## Requested discussions / Any other business

- **Adriaan: On the structure of the hardware configs**
https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/421

  - limitations to the existing configs:
    - mostly in how it hard-codes certain mappings that should not be fixed in the config
    - and that it does many other things besides only the hardware mapping

  - Goals
    - information sharing/alignment of developments. 
      - Ensure we are all aware of what is going on 
      - Ensure everyone is aware of what is already there and what previous considerations were. 
      - Avoid double work. 
  - give some kuddos

  ```
  Adriaan:

  setting up hw config file is hard

  complex
  not very well documented
  not very well validated


  Formulate what the structure / config should look like

  Idea of using pydantic configs
  - Comes with functionality to create from and export to json 


  Structure is not defined, do we want to generalize it?
  - Adriaan: simply making a pydantic config to reflect and allowing to test existing structure, already great step ahead

    - problems: leading object is piece of hardware
      - no dyn ass of port/clock to elements
    
      - too much information in config file
        - should be only connectivity, only p/c to output
        - should not contain no mixer and gain settings


     Slava: let's go for more modular, see example quantify-scheduler!403 
     - https://quantify-quantify-scheduler--403.com.readthedocs.build/en/403/planned/redesign.py.html
     - we need to be able to swap control hardware


  Adriaan: for clear path forward:

  1. pydantic validator for exisitng qblox config
  2. use that to see with all the concepts we are expressing
    - connectivity
    - latency corr
    - mixer settings

  ```

- **Robert / Kelvin: On the use of lists vs numpy arrays**
  - enforce the use of numpy arrays?
  - might need to think of a way to use [`quantify_core.data.handling.DecodeToNumpy`](https://gitlab.com/quantify-os/quantify-core/blob/main/quantify_core/data/handling.py#L35-35) more effectively

  ```
  On conversion of json to numpy

  Slava: Pydantic is dict on steriods 
  convert to pydantic instead our own 
  built-in serialization and deserialization

  Kelvin: compatible with qcodes?

  Adriaan: q device obj + q device elements: objects to manage knowledge of the system (comp with qcodes)
  - The q device obj then generates format that compiler understands (pydantic)

  Good news: 
  - this was stirred up from distortion corrections config in the hardware config
  - now that Koen is migrating hw config to pydantic, this would be taken care of
  ```

- **Adith / Edgar: Why determine abs timing under device_compile, move up to qcompile?**
  - https://gitlab.com/quantify-os/quantify-scheduler/blob/f4812c794d230ea1c80ea2af4c82106b43aeba2e/quantify_scheduler/compilation.py#L491-491
  - _It was agreed to add it to qcompile as well, to be executed when no device config is supplied. Edgar will create a quick MR for this_

- **Christiaan**: on granularity and need of shifting/padding operations

  ```
  Zhinst:
  waveforms need to be multiple of 32
  so need to shift samples

  something that we should fix hardware agnostic

  qblox it is 4 ns, multiple of 4

  Adriaan: there are limitations in the backend, but they are not fundamental
  play waveform before other has ended, is it cut off?

  always add buffer, as it will be cut off when the next waveform is played
  ``` 

- **Xiang**: 
  - two new members will join Quantify
  - will work on backend for quantumctek ezQ control hardware (the quantum supremacy pioneers)
  - ezQ is AWG built internally at quantumctek

## Pending topics (for next DMs)
_None_


