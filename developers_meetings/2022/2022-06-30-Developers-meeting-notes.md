## Highlights
- Release of Quantify-core [v0.6.1](https://gitlab.com/quantify-os/quantify-core/-/releases/v0.6.1)
- A reply there but Kelvin still needs to fully read through it. (https://github.com/QCoDeS/Qcodes/discussions/4289)

## Breaking changes
- Quantify-core uses Myst-Markdown for future tutorials (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/350)

## Quantify-scheduler
- Thanks Michel for helping to review [MR399](quantify-scheduler!399).
- Discussions on acquisition indexing available here. (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/36)

#### Merged MRs (highlights)
-  Qblox: Fix cluster-compatibility in converting old hw_config to new hw_config spec [MR415](https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/419)

## Quantify-core
- Release of Quantify-core [v0.6.1](https://gitlab.com/quantify-os/quantify-core/-/releases/v0.6.1)


#### Merged MRs (highlights)
- Convert documentation to MYST markdown [MR350](https://gitlab.com/quantify-os/quantify-core/-/merge_requests/350)

## Requested discussions / Any other business

- Qiskit
  - in august/september, contributor joining that will be creating interface between Quantify and Qiskit
  - Zihao: interested in joining in discussion on this topic then

- quantify-scheduler!410
  - Christian/Luc tested it but couldn't see yet if it was improvement (due to using old transmon element)
  - Damaz is still to review

- What goes into rst and what in md:
  - docstrings are still in rst format
  - rest in md format
  - be sure to check quantify-core!350
  - **for now only for core! scheduler will follow, but we need to fix API reference issue first (see below)**

- API reference scheduler still broken but will cont'd on soon (Robert/Edgar): quantify-scheduler#303 quantify-scheduler!413


## Pending topics (for next DMs)
_None_

