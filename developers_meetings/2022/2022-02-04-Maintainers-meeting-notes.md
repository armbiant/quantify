## Present:

### Maintainers and Project Owners

- [ ] Kelvin
- [ ] Damien
- [ ] Slava
- [ ] Adriaan (PO)
- [ ] Jules (PO)

### Community

- [ ] Jordy
- [ ] Adam
- [ ] Diogo
- [ ] Gijs
- [ ] Damaz
- [ ] Vraj
- [ ] Tobias
- [ ] Achmed

## Introduction new member:

## Highlights:

## Breaking changes:
- No breaking changes!

### Quantify-scheduler:
- Present milestones. (https://gitlab.com/quantify-os/quantify-scheduler/-/milestones)

#### Merged MRs (highlights)
- Introduce Pydantic-based model for data structures 
  - https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/341
    
Minor:
- Add "operation_type" to schema (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/345)
- Add data argument to staircase pulse (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/335)
- Qblox backend - enable RF output switch at start of program (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/344)
- Fix for deprecation pandas DataFrame.append (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/347)
  
### Quantify-core:
- Please mind we set the deadline for a 1.0 release before the 14th of March.
  - Milestones: https://gitlab.com/quantify-os/quantify-core/-/milestones/9

#### Merged MRs (highlights):

- Simplify Gitlab CI workflow "when" definition
  - https://gitlab.com/quantify-os/quantify-core/-/merge_requests/292


## Requested discussions:

- No requests this week

## AOB:

- Open the floor.

## Pending topics (for next MMs):

- No pending topics
