## Highlights

### Breaking changes
- Qblox ICCs - Hotfix for storing scope acquisition (broken by quantify-scheduler!432) (quantify-scheduler!470)

## Quantify-scheduler

### Merged MRs (highlights)

## Quantify-core

### Merged MRs (highlights)

## Requested discussions / Any other business

### Conclusion to the discussion on SpecPulse
- https://gitlab.com/quantify-os/quantify-scheduler/-/issues/330
- Intended outcome: conclusion on whether we need it and where to place the code

Konstantin (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/330#note_1101844539):
1. rename to SpectroscopyOperation
2. nv_library (leaving it in the circuit library): one lib for each qubit arch
3. leave gate_library unchanged

Adam: uses it too for transmon, so nv_libary not best place, new spectroscopy lib perhaps

Slava: is not so important for normal user, as they interact with DeviceElement

On review:
Slava: plz insert reverse compatibilty aliases where required (e.g. `@deprecated`)

Qblox SE will review before next dev meeting

### Hardware config schema/formalization
- Outcome from Hardware config meeting on Tuesday (13/9/2022)
- https://gitlab.com/groups/quantify-os/-/epics/1: Formalize hardware configuration
  - `CompilationConfig: {version, DeviceParameters, HardwareOptions, Connectivity}`
- **Intended outcome:** how to move this forward, essentially expectations around who does what and when

Edgar: 
- agreed with Adriaan he will pick up hooking up on high level into the CompilationConfig (but he's going on holiday first)
- Qblox and OQS will then take a look at gradually moving parts to their right location, while retaining backwards compatibility
  - That is, current hardware config will be allowed to be inserted as a whole into Connectivity for backwards compat

Konstantin: where do different compilation nodes get their info

Slava: shared, HardwareOptions holds compilation options for possibly many different compilation nodes

### Kelvin/Kesnia: Follow up discussions on Qiskit-Quantify available
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/465
- Have a short look at the prototype interface. Ksenia to show and explain the interface.

### Slava: request to remove functionality from `quanify-core`
- There are two MRs that remove functionality, that is likely not used by anyone in practice.
  - https://gitlab.com/quantify-os/quantify-core/-/merge_requests/378
  - https://gitlab.com/quantify-os/quantify-core/-/merge_requests/379
- **Intended outcome:** if nobody objects, merge this MRs after the meeting.

### Extra discussion


## Pending topics (for next DMs)

### Public API (No follow up yet since 2022-08-4) [Owner: @rsokolewicz (guidelines, listing)]
Also see https://quantify-hq.slack.com/archives/C02DE4ZENNQ/p1659630403947179?thread_ts=1659541477.414369&cid=C02DE4ZENNQ

We need to discuss about plans for making formal the public APIs so that we do not expose everything within Quantify as a public API. This is so that any internal implementations would not break a dependent package. (quantify-scheduler#313)
- What is public (and thus breaking, in need of using deprecation scheme upon change), what is private
- Make whole modules private?

**Decision**:
- Whatever user needs is public.
- Guideline: "private, unless..."
- Process: minor changes incorporated to actual work.

### Line lengths (No follow up yet since 2022-08-4) [Owner: @damazdejong]
Requested by Damaz - A proposal to improve the way we work with Quantify styles.

Damaz: Codacy is complaining when Black fails to reformat a string to 88 chars.
Zhihao: Docstrings are not formatted at all.
**Decision**: Move the discussion online.

### Intermediate representation (No follow up yet since 2022-08-4) [Owner: @gtaifu]
Xiang: we are interested in discussion on intermediate representation, especially in the context of Quingo compiler (https://dl.acm.org/doi/10.1145/3483528).
**Decision**: Xiang will make slides about Quingo and share them in Slack, later we will schedule a slot for presentation.
