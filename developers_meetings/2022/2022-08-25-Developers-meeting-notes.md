## Highlights

### Breaking changes

- [quantify-core!286](https://gitlab.com/quantify-os/quantify-core/-/merge_requests/286) likely has introduced backwards
  incompatibility. OrangeQS is investigating internally, for now we cosider it rather an issue than a breaking change.

## Quantify-scheduler

### Merged MRs (highlights)

- [!420](https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/420) introduces profiling the schedule.
  We don't recommend to rely too much on it yet, because it will be refactored ([#320](https://gitlab.com/quantify-os/quantify-scheduler/-/issues/320)).

## Quantify-core

### Merged MRs (highlights)

- Improvement in handling SI prefixed (thanks @peendebak!) [!365](https://gitlab.com/quantify-os/quantify-core/-/merge_requests/365)
- [!286](https://gitlab.com/quantify-os/quantify-core/-/merge_requests/286) improved `lmfit` model saving in analysis.

## Requested discussions / Any other business

- Zhihao @r3b1r7h: "Could I have a brief show for the my refactoring about zhinst backend in tomorrow's meeting?"
  - Follow ups:
    - Feedback: open Draft to get initial high level feedback 
    - Slava, on reviewing: 
      - will mostly be on the level whether it works or not
      - we will need to involve other zhist users (e.g. @christiandickel8 and crew)


### Extra discussion

- Adam/Fokko: insmon queries instruments all the time, causing comm problems 
  - Seems caused by quantify-core!324
  - Ticket created to track discussion on issue and follow-ups quantify-core#327
    - original slack thread https://quantify-hq.slack.com/archives/C02DE4ZENNQ/p1661427372055249
  - Follow-ups:
    - Try reverting (has now been attempted, but didn't work)
    - Is it in the release? Yes, since core release 0.6.0 (https://gitlab.com/quantify-os/quantify-core/-/issues/327#note_1078265681)
    - We will need to patch this issue on `main`, and then plan the actual fix
      - Also given that this is released code, we may need to do a patch release 
      - @rsokolewicz is taking care of the patch 
    - We need a checkbox _Check on hardware_, @ereehuis will create MRs for that 


## Pending topics (for next DMs)

### Do we need a SpecPulse operation? (Postponed at least till 2022-08-31 due to vacations)

Fokko introduces a discussion point to be discussed this week. There are plans to include a spec pulse operation to Quantify, but he feels treating it as an operation is redundant and should be treated as a pulse instead.

### External MR (No follow up yet since 2022-08-11)
- Looks like Kelvin needs to review the MR on the zhinst backend. (quantify-scheduler!399)

### Public API (No follow up yet since 2022-08-4) [Owner: @rsokolewicz (guidelines, listing), @ereehuis (checkboxes MRs)]
Also see https://quantify-hq.slack.com/archives/C02DE4ZENNQ/p1659630403947179?thread_ts=1659541477.414369&cid=C02DE4ZENNQ

We need to discuss about plans for making formal the public APIs so that we do not expose everything within Quantify as a public API. This is so that any internal implementations would not break a dependent package. (quantify-scheduler#313)
- What is public (and thus breaking, in need of using deprecation scheme upon change), what is private
- Make whole modules private?

**Decision**:
- Whatever user needs is public.
- Guideline: "private, unless..."
- Process: minor changes incorporated to actual work.

Include new checkboxes on MR?
- Changed functionality: public API so should we deprecate? => checkbox: n/a, done
- New functionality: should it be private? => checkbox: n/a, done
- Should be tested on hardware? => checkbox: n/a, done

**Decision**:
- Everyone is welcome to improve template by sending an MR, but probably this will be done by maintainers.
- "Manual test" checkbox is needed.

### Line lengths (No follow up yet since 2022-08-4) [Owner: @damazdejong]
Requested by Damaz - A proposal to improve the way we work with Quantify styles.

Damaz: Codacy is complaining when Black fails to reformat a string to 88 chars.
Zhihao: Docstrings are not formatted at all.
**Decision**: Move the discussion online.

### Intermediate representation (No follow up yet since 2022-08-4) [Owner: @gtaifu]
Xiang: we are interested in discussion on intermediate representation, especially in the context of Quingo compiler (https://dl.acm.org/doi/10.1145/3483528).
**Decision**: Xiang will make slides about Quingo and share them in Slack, later we will schedule a slot for presentation.
