## Highlights

- Quantify-Core is at 0.6.4 (quantify-core!383)


### Breaking changes

## Quantify-scheduler
- SpectroscopyOperation for NV centers (quantify-scheduler!471)

### Merged MRs (highlights)

## Quantify-core

### Merged MRs (highlights)
- Save dataset name in dataset filename (quantify-core!388)
- Concatenate datasets with different names (quantify-core!389)
- Fix CI repo branch after `staging` has been merged (quantify-core!395)
- Measurement control experiment data (quantify-core!39)
- Concatenate processed datasets (quantify-core!394)

## Requested discussions / Any other business

### Convention for submodule names in device elements- Konstantin Lehmann & Hiresh Jadoenathmissier
Idea: same name as gates, but prefixed by _. Need to make sure that we still render documentation for these classes. See also [quantify-scheduler#343](https://gitlab.com/quantify-os/quantify-scheduler/-/issues/343) and [quantify-scheduler#346](https://gitlab.com/quantify-os/quantify-scheduler/-/issues/346)

Decision:
- keeping device specific classes public (not to use underscores),
  because they must be visible to the users.
- Developers should come up with descriptive names so that there is no name collision.
  For example appending pulse shapes after the operation name (for example RxyDrag, RxyHermite).

### Reflection session - Robert Sokolewicz
"I was thinking that it would be good to host one meeting where we sit together to reflect on how we develop quantify. Anything can be discussed: the role of maintainers/developers, how merge requests and issues are handled, about the use of the developer's slack channel, about the developer's meeting... It's really a moment for reflection and to see if there are some frustrations or emotions that need to be addressed and to see if we can improve our workflow/politics/...

One example that could be discussed is how the nv center spectroscopy operation MR was handled. It took quite long and caused some frustrations and I'm sure we could've done a better job if we approached it differently."

**This is an important issue, but not urgent, we will discuss this later, not on todays meeting.**

### Extra discussion

Users feedback on biggest weakneeses, important missing features.

Weaknesses
- Documentation review is needed, missing things in the documentation in general.
- Error messages come from very deep in the code,
  we should make it more expressive, raising error at the higher level.
- Multi-qubit measurement, and data acquisition features are difficult to use.
- Difficult to write hardware configuration files. It's easy to make mistakes.
- There's a lot of inherent assumptions that quantify is for transmons.
  It restricts, and sometimes makes interface counterintuitive.
- Not enough encapsulation for data in classes,
  and quantify uses too generic data structures, when stricter types are needed.

Features suggestions
(Note, the priority is to make current feature set better and complete, not to add new features.)
- Store transmonelement parameters, maybe logging capability for this, or server-client.
- Creating map of quantify as a documentation: all the classes and their purposes.
- Multi-user experiments, maybe running quantify as a server, which multiple people can access.

### Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

### Public API (No follow up yet since 2022-08-4) [Owner: @rsokolewicz (guidelines, listing)]
Also see https://quantify-hq.slack.com/archives/C02DE4ZENNQ/p1659630403947179?thread_ts=1659541477.414369&cid=C02DE4ZENNQ

We need to discuss about plans for making formal the public APIs so that we do not expose everything within Quantify
as a public API. This is so that any internal implementations would not break a dependent package. (quantify-scheduler#313)
- What is public (and thus breaking, in need of using deprecation scheme upon change), what is private
- Make whole modules private?

**Decision**:
- Whatever user needs is public.
- Guideline: "private, unless..."
- Process: minor changes incorporated to actual work.

### Line lengths (No follow up yet since 2022-08-4) [Owner: @damazdejong]
Requested by Damaz - A proposal to improve the way we work with Quantify styles.

Damaz: Codacy is complaining when Black fails to reformat a string to 88 chars.
Zhihao: Docstrings are not formatted at all.
**Decision**: Move the discussion online.

### Intermediate representation (No follow up yet since 2022-08-4) [Owner: @gtaifu]
Xiang: we are interested in discussion on intermediate representation, especially in the context of Quingo compiler
(https://dl.acm.org/doi/10.1145/3483528).
**Decision**: Xiang will make slides about Quingo and share them in Slack, later we will schedule a slot for presentation.

