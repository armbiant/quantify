
_FYI: condensed meeting template in agreement with Kelvin_ 

## Highlights
_None_


## Breaking changes
_None_


## Quantify-scheduler
- On request: [Review milestones](https://gitlab.com/quantify-os/quantify-scheduler/-/milestones)
- Release 0.6.0 is underway (https://gitlab.com/quantify-os/quantify/-/issues/20)
- Kelvin still needs to remove the intersphinx mapping of the zhinst drivers (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/283)  
_Follow up: attempted but not so straightforward sadly, possible alternative workaround is building the zhinst RTD ourselves_

#### Merged MRs (highlights)
- Numerically defined pulses are implemented (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/157). Enables waveform corrections.
- Utilities from quantify-core has been migrated to quantify-scheduler (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/357)


## Quantify-core
- Mentioning a cool dataset cache (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/307)
- Cause of the remote plotmon crashes has been identified and fixed. (https://gitlab.com/quantify-os/quantify-core/-/issues/285)

#### Merged MRs (highlights)
_None_


##  Requested discussions / Any other business
- Amber: Help from maintainer to review draft unified Quantify landing page  
_Edgar to do first scan, https://orangeqscom.sharepoint.com/:w:/s/MITQuantify/EeC_AaDu_H5Bs7hrIMPyGIgBwfALn-SyOcmyiEnyF03BKw?e=kHpsAY_
- Adam: found the bug! (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/284#note_870363506) MR is out: https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/365
- Zhihao/Xiang: 
  - got the new zhinst-qcodes driver to work (for IQ mixer calibration experiment)
  - in the future may update zhinst backend to use new driver, but no clear plan for that yet
  - Kelvin: in case of any zhinst backend MR for using new driver, please make sure to add in case it is tested on the zhinst hardware already - helps in review 
  - (also, Kelvin will lose access to zhinst hardware in 2 weeks) 
- Edgar: test qcodes 0.33.0 see if it resolves RTD issue  
_No. https://gitlab.com/quantify-os/quantify-core/-/issues/296#note_870627758_


## Pending topics (for next MMs)
- _Edgar (not mentioned in the meeting itself): Should we start calling these Developer Meetings instead_?
