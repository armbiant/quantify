## Highlights

### Breaking changes

- Deprecate the use of the data argument for Operations, Resources and Schedulables (quantify-scheduler!455)

## Quantify-scheduler

### Merged MRs (highlights)

- Qblox ICCs - Only activate markers and LOs of used outputs to prevent noise (QAE-326) (quantify-scheduler!474)


## Quantify-core

### Merged MRs (highlights)

## Requested discussions / Any other business

### Zhinst Parser error occurred during parsing of command table JSON loaded to AWG 3
- Martijn Zwanenburg running into:
  ```
  We have:

  zhinst                        21.8.20515
  zhinst-hdiq                   1.0.0
  zhinst-qcodes                 0.1.4
  zhinst-toolkit                0.1.5

  The error looks something like this:

  Error2022/9/5 10:59:09 41681363299603622 Parser error occurred during parsing of command table JSON loaded to AWG 3

  Two things I've noticed:
  - We never experienced this when we were using one channel of the AWG at the time. The error only seems to pop-up when you compile waveforms to at least two channels on the AWG.
  - Turning the AWG off and on resolves the error. If you run the exact same experiment with the exact same waveform after restarting the AWG, the error is no longer there.
  ```
- **Desired outcome**: Kelvin hoping that some other Zhinst users (@christiandickel8 et al, @gtaifu/@r3b1r7h) are able to chip in some wisdom



### Final (:fingers_crossed:) conclusion to the discussion on SpecPulse
- https://gitlab.com/quantify-os/quantify-scheduler/-/issues/330
- Intended outcome:
	- Get approval for spectroscopy pulse implementation below
	- alternatively: find out how to best reach conclusion without further delay
- Latest suggestion:  
Konstantin (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/330#note_1105029474):
  1. rename to SpectroscopyOperation
  2. shared_library (leaving operation on circuit level)
  3. leave gate_library unchanged
- **Conclusion**:
  1. There will be 3 different kind of operations:
     - logical (can be represented by linear transformation or reset or measurement),
     - shared (cannot always be represented by linear transformation, but widely used, makes sense on lot of different kinds of device),
     - native (fully defined on the device).
     There might be a small compilation step between shared to native.
     We split up the current native level library, it will also accomodate shared operations for now.
  2. SpectroscopyOperation will live in the shared library.
  3. Still did not decide how to implement the shared operation library *long-term*.

### Extra discussion

- Handling deprecated code suggestions:
  - The developer who deprecates code should make sure to remove deprecated code from implementation (not interface)
  - If implementation (not interface) uses deprecated code, automatic tests should fail
  - We should still test with unit tests deprecated functionalities, we need to support them until removed

### Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

### Public API (No follow up yet since 2022-08-4) [Owner: @rsokolewicz (guidelines, listing)]
Also see https://quantify-hq.slack.com/archives/C02DE4ZENNQ/p1659630403947179?thread_ts=1659541477.414369&cid=C02DE4ZENNQ

We need to discuss about plans for making formal the public APIs so that we do not expose everything within Quantify
as a public API. This is so that any internal implementations would not break a dependent package. (quantify-scheduler#313)
- What is public (and thus breaking, in need of using deprecation scheme upon change), what is private
- Make whole modules private?

**Decision**:
- Whatever user needs is public.
- Guideline: "private, unless..."
- Process: minor changes incorporated to actual work.

### Line lengths (No follow up yet since 2022-08-4) [Owner: @damazdejong]
Requested by Damaz - A proposal to improve the way we work with Quantify styles.

Damaz: Codacy is complaining when Black fails to reformat a string to 88 chars.
Zhihao: Docstrings are not formatted at all.
**Decision**: Move the discussion online.

### Intermediate representation (No follow up yet since 2022-08-4) [Owner: @gtaifu]
Xiang: we are interested in discussion on intermediate representation, especially in the context of Quingo compiler
(https://dl.acm.org/doi/10.1145/3483528).
**Decision**: Xiang will make slides about Quingo and share them in Slack, later we will schedule a slot for presentation.
