## Epics (APSMM23 release)

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Edgar)
  - https://gitlab.com/quantify-os/quantify-scheduler/-/issues/332 Update CompilationConfig with high-level structure
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Daniel)
- **Parallel work**:
  1. [milestone 3](https://gitlab.com/groups/quantify-os/-/milestones/3)  Quantify Platform release aka The Website :tm: (remaining: website + RTD replacement)
  1. Remove deprecated code / usage
  1. Clean up open tickets (**247** open and counting [board](https://gitlab.com/groups/quantify-os/-/boards))
  1. Fast feedback in Quantify (first MRs) (Qblox)
  1. https://gitlab.com/quantify-os/quantify-scheduler/-/issues/320 ProfiledScheduleGettable needs refactoring (Qblox)
  1. https://gitlab.com/quantify-os/quantify-scheduler/-/compare/main...QAE-472-Draft-parallel-hardware-prepare-support (Qblox)
  1. ...
 
## Highlights

Compiled a Quantify Deprecated Code Suggestions notebook, available via https://gitlab.com/quantify-os/quantify-scheduler/-/snippets/2462483, shows:
- Enable DeprecationWarnings
- 1. Qcompile => SerialCompiler
- 2. Qblox Hardware Configuration
- 3. TransmonElement => BasicTransmonElement


### Breaking changes

## Quantify-scheduler

### Merged MRs (highlights)

- Charge Reset operation for NV qubits (quantify-scheduler!496)
- Refactor `test_acquisitions`: Move to `mock_setup_basic_transmon_with_standard_params` and replace `qcompile` by `SerialCompiler` (quantify-scheduler!516)
- Resolve "Nondescriptive error message when confusing QRM and QCM in hardware config" (quantify-scheduler!519)
- Make pulse_diagram_matplotlib compatible with quantify-core>0.6.4 (quantify-scheduler!517)
- Documentation build will now fail after a CellExecutionError (QAE-665) (quantify-scheduler!514)
- _Ability for user to change the frequency of the clock in schedule (QAE-324) (quantify-scheduler!509) => merged into https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/512_

## Quantify-core

### Merged MRs (highlights)

- Skip creation of figures in base analysis class (quantify-core!406)
- Documentation build will now fail after a CellExecutionError (QAE-665) (quantify-core!407)

## Requested discussions / Any other business

### Clock resource verification (and eventual addition to the schedule)
@mafaldajg  https://gitlab.com/quantify-os/quantify-scheduler/-/issues/371

Not being done in some cases:  
- I would like to present the 2 separate issues that arise, which I believe have a mutual solution.  
- => In my opinion, there are 2 possible approaches for a solution and I would like to have your opinion on which implementation would work better. 

```
Possible solutions:
- remove the clock verification from compile_from_circuit_to_device, add new function for it
- or add to other existing function that we already pass through

=> Slava, Adriaan: add to separate compilation step, make it a new node, see graph compilation/SerialCompiler
Mafalda, Damaz: essentially approach 1, great
```

### Schedule time quantization
@adamorangeqs https://gitlab.com/quantify-os/quantify-scheduler/-/issues/372

Intro / Discussion:
- We have a frequently occurring problem in the lab where we get errors due to pulses not starting on the 4 ns grid
- I would like to propose we implement some automatic quantization of operation times to fix this.
- => The idea is that if an operation happens to fall outside of the grid, the scheduler could then automatically make the operation 'snap to to the grid'
  - _This functionality is similar to how a lot of graphics or music-making software works, where the user can place an element somewhere and it automatically gets fixed onto a grid._ :-)

```
Damaz: so something that auto puts back on the grid
Two sol:
- discretizer, move left and right 
- approximate your pulse, resample it so it fits on the grid (don't want to mess with timing by default)

Adriaan: changing things that user is not aware of, potential for major fu
- One solution, explicit warning messages

Adam: follow-up: 
- host design meeting
```

### Quantify Retro part deux (30 min)
@rsokolewicz

Continue our reflection/retrospection discussion where we left off: 
- Two weeks ago [2022-11-09-Developers-meeting-notes.md](2022-11-09-Developers-meeting-notes.md) we talked about A) reviewing process for MRs and B) collaboration between Qblox and OQS, but there are still many post-its and topics that weren't discussed. 
- => Feel free to add more post-its to the board: https://miro.com/app/board/uXjVPFQm28E=/


## Extra discussion

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)  
_**Cleaned up long open topics, for previous list see [2022-11-17-Developers-meeting-notes.md](2022-11-17-Developers-meeting-notes.md)**_

### Formalizing how hardware parameters are updated inside schedules [organized by @djweigand and/or @damazdejong]

### ~Public API [@rsokolewicz]~
_As discussed/decided on the developer's meeting of 27/10/22 [2022-10-27-Developers-meeting-notes.md](https://gitlab.com/quantify-os/quantify/-/blob/main/developers_meetings/2022/2022-10-27-Developers-meeting-notes.md), making a proper overview of modules/classes/methods that should become private/public is too much work (e.g. 100 vs 600 classes in core and scheduler). We will scope it down, and follow the guidelines set here by making the internals of compilation private and leaving the rest alone. We did not yet decide how this will be made private, i.e. by prepending the module/classes with a `_`, or by explicitly stating the public interface via `__all__ = [...]` at the top of each module._
 
_For the rest, we continue as before, whenever new code is added to the code base the author will decide whether this should be public or private.
(https://gitlab.com/quantify-os/quantify-scheduler/-/issues/313#note_1184261786)_


