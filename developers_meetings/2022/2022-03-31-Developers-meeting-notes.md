## Highlights

Zhinst-qcodes documentation links fixed by changing to newly hosted https://docs.zhinst.com/zhinst-qcodes/en/v0.1/ (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/375, https://gitlab.com/quantify-os/quantify-core/-/merge_requests/318)
  
  

## Breaking changes
Michel: In !372, zhinst_hardware_config, `triggers` key are now renamed to `trigger`. Value type also changed from `List[int]` to `int`.

  
## Quantify-scheduler

- New compilation backend has unwanted clocks bug for qblox backend. (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/278, https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/371). Fix testing on actual hardware still in progress (_mentioned before in 2022-03-24 meeting notes_)

- New qblox-instruments driver: Release early next week containing updated InstrumentCoordinator Qblox components (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/377)
  - Firmware, driver, quantify needs to be updated.
  

#### Merged MRs (highlights)

- Resolve "Marker and triggers not configured properly in ZI backend" (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/372).
  - _So, we can close Marker and triggers not configured properly in ZI backend (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/264)?_
    - Need to re-open since documentation not complete.
  
  

## Quantify-core
_None_

  

#### Merged MRs (highlights)
_None_
  

## Requested discussions / Any other business
- Qblox Cluster. Some experiences (https://github.com/pyvisa/pyvisa-py/pull/292)
- Michel: Clarify contribution guidelines.


## Pending topics (for next DMs)
