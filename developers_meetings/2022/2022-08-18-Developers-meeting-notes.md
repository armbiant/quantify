## Highlights
- Quantify-scheduler implements a new graph based compilation. (quantify-scheduler!407)

## Breaking changes
- Quantify-scheduler is now a graph. (quantify-scheduler!407)

## Quantify-scheduler
- Documentation now uses MYST markdown. (quantify-scheduler!452)

#### Merged MRs (highlights)
_None_

## Quantify-core
- Various bugfixes. (quantify-core!370, quantify-core!368, quantify-core!367, quantify-core!363)

#### Merged MRs (highlights)
_None_

## Requested discussions / Any other business

### Extra discussion
- Fokko introduces a discussion point to be discussed this week. There are plans to include a spec pulse operation to Quantify, but he feels treating it as an operation is redundant and should be treated as a pulse instead.

## Pending topics (for next DMs)

### External MR (No follow up yet since 2022-08-11)
- Looks like Kelvin needs to review the MR on the zhinst backend. (quantify-scheduler!399)

### Public API (No follow up yet since 2022-08-4)
We need to discuss about plans for making formal the public APIs so that we do not expose everything within Quantify as a public API. This is so that any internal implementations would not break a dependent package. (quantify-scheduler#313)
- What is public (and thus breaking, in need of using deprecation scheme upon change), what is private
- Make whole modules private?

**Decision**: 
- Whatever user needs is public.
- Guideline: "private, unless..."
- Process: minor changes incorporated to actual work.

Include new checkboxes on MR?
- Changed functionality: public API so should we deprecate? => checkbox: n/a, done
- New functionality: should it be private? => checkbox: n/a, done
- Should be tested on hardware? => checkbox: n/a, done

**Decision**:
- Everyone is welkome to improve template by sending an MR, but probably this will be done by maintainers.
- "Manual test" checkbox is needed.

### Documentation (No follow up yet since 2022-08-4)
_Edgar: propose to move this to next week, some of the math statements aren't rendering (https://quantify-quantify-scheduler--349.com.readthedocs.build/en/349/autoapi/quantify_scheduler/operations/gate_library/index.html#quantify_scheduler.operations.gate_library.X90)_
There is an open merge request related to adding documentation to gates (see quantify-scheduler!349).

### Line lengths (No follow up yet since 2022-08-4)
Requested by Damaz - A proposal to improve the way we work with Quantify styles.

Damaz: Codacy is complaining when Black fails to reformat a string to 88 chars.
Zhihao: Docstrings are not formatted at all.
**Decision**: Move the discussion online.

### Intermediate representation (No follow up yet since 2022-08-4)
Xiang: we are interested in discussion on intermediate representation, especially in the context of Quingo compiler (https://dl.acm.org/doi/10.1145/3483528). 
**Decision**: Xiang will make slides about Quingo and share them in Slack, later we will schedule a slot for presentation. 

