
## Highlights
- Quantify-scheduler Read-the-Docs API reference is broken (once again), working on replacing by `sphinx-autoapi` generated API ref (for both sched and core)
  - Preview (quantify-scheduler!398): https://quantify-quantify-scheduler--398.com.readthedocs.build/en/398/autoapi/index.html
  

## Breaking changes
- Dynamic sequencer allocation is added for the Qblox backend. (quantify-scheduler!328):
  - breaking change for the hardware config (`seq0` => `portclock_configs`) but we call a conversion function (for now), see https://gitlab.com/quantify-os/quantify-scheduler/-/wikis/Qblox-backend:-Dynamic-Sequencer-Allocation


## Quantify-scheduler
- Measurement with optimal readout via numerical integration weights has a design and a draft MR has started (quantify-scheduler!391, quantify-scheduler#295)
- Drafts for modular handling of distortion corrections (quantify-scheduler!388) and latency corrections (quantify-scheduler!395) in Qblox backend

#### Merged MRs (highlights)
- Fix for supplying negative NCO phase when using the Qblox backend. (quantify-scheduler!393)
- Moved MockLocalOscillator definition from tests to `helpers.mock_instruments.MockLocalOscillator` (quantify-scheduler!392)
- Fix for outputting signals on even output paths of qblox hardware in real_output_mode (!397)
   

## Quantify-core
- New release of quantify-core planned for next week :rocket: (by Slava) 
- `load_snapshot` should now support decoding from list to numpy arrays. Use the argument `list_to_ndarray=True` when using the `load_snapshot` function. (quantify-core!343)
  
#### Merged MRs (highlights)
- Adds a numpy decoder for the json deserialization (quantify-core!342)
  

## Requested discussions / Any other business
- Fokko: new quantify-scheduler tutorials (quantify-scheduler!336)


## Pending topics (for next DMs)
_None_

