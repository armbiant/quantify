## Highlights
- UnitaryHack is underway. Do remember to sign up to win 100 USD bounties (for non-OrangeQS and non-Qblox members)! (https://unitaryhack.dev/projects/quantify-scheduler/) (https://unitaryhack.dev/projects/quantify-core/)

## Breaking changes
- Remove an Extra G_amp Factor From The Derivative Pulse of DRAG (quantify-scheduler!406)
- QCoDeS are pinned at < 0.34.0 due to name convention.

## Quantify-scheduler
- MR (quantify-scheduler!399) still to be reviewed.

#### Merged MRs (highlights)
- Remove an Extra G_amp Factor From The Derivative Pulse of DRAG (quantify-scheduler!406)
- Pin qcodes < 0.34.0 due to https://gitlab.com/quantify-os/quantify-scheduler/-/issues/300 (quantify-scheduler!409)
- Fix compilation of ShiftClockPhase in Qblox backend (broken by merge of !328) (quantify-scheduler!404)
- Update requirements.txt (quantify-scheduler!405)

## Quantify-core
_None_
  
#### Merged MRs (highlights)
- Pin qcodes < 0.34.0 due to https://gitlab.com/quantify-os/quantify-scheduler/-/issues/300 (quantify-core!348)
- Make BaseAnalysis.create_figures() create figures again (quantify-core!347)
  

## Requested discussions / Any other business
- (Maintainers) Convention for QCoDeS Instrument names. To comply with qcodes >= 0.34.x. Affects Edge, and port-clock mapping and possibly more. https://gitlab.com/quantify-os/quantify-scheduler/-/issues/300


  Affects Edge (instrument) names, portclock key naming is not affected

  Possible solutions:
  1. Account for conversion and internally expect converted name
  2. Change of naming of qubits: always end in number
  3. No connector, simply q0q1
  4. Remove use of instrument name, i.e. use of find instrument by name in quantify
  5. No underscores! in any instrument name
  6. Never parse back: We shouldn't use name/label to find constituents

  **[Follow-up]** Maintainers need to:
  - join Qcodes slack (email requesting access sent, see https://qcodes.github.io/Qcodes/help.html)
  - choose one from approaches listed above



- (Zhihao) Redundant `t0` in waveform? (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/410#note_992861449)

  Outcome of discussion:
  - t0 should be included in waveform directly, not in acquisition_info
  - port & clock should not be included in waveform

  ```python
        waveform_i = {
            "t0": t0,
            "duration": duration,
            "wf_func": "quantify_scheduler.waveforms.square",
            "amp": 1,
        }

        ...

        if data is None:
            data = {
                "name": "SSBIntegrationComplex",
                "acquisition_info": [
                    {
                        "waveforms": [waveform_i, waveform_q],
                        "clock": clock,
                        "port": port,
                        "duration": duration,
                        "phase": phase,
                        "acq_channel": acq_channel,
                        "acq_index": acq_index,
                        "bin_mode": bin_mode,
                        "acq_return_type": complex,
                        "protocol": "ssb_integration_complex",
                    }
                ],
            }
  ```


- (Adriaan) Proposal to remove jupyter sphinx auto convert extension (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/301)

  - **[Follow-up]** Maintainer decision
    - ~Change nothing~
    - Keep rst
    - Convert to myst markdown (markdown on steriods that allows for jupyter-execute, https://myst-parser.readthedocs.io/en/latest/)

- (Damaz) Reviewing of quantify-scheduler tutorials (which do have draft in title I take it all back Fokko https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/336): 
  - **[Follow-up]** OQS can't make a promise on when they'll be able to review right now, but will be able to update us next meeting

- (Adam)
  - **[Follow-up]** acquisition indexing for multiple qubits: adam will plan a meeting for next week (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/192)
    - Basically the acq_channel parameter is not user friendly because it comes up in multiple parts of the code, and the user has to manually ensure that everything is consistent, which is difficult. Ideally, the numbering of acquisition channels should be handled automatically
  - Something in the backend to account for dc flux crosstalk correction (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/302), we don't have anything for it right now
    - Could handle this similarly like distortion corrections
    - Adam needs it asap, he'll try to hack something in, discuss next week in nanonisq meet to come up with approach for longer term (similar to distortion corrections)



## Pending topics (for next DMs)


**[Follow-up]**
- Maintainers choose one from approaches listed above to omit edge naming issue (due to qcodes 0.34.x)
- Maintainer decision on jupyter sphinx auto convert extension replacement
- OQS to follow up on when possible to review scheduler tutorials 
- Adam to plan meeting on acquisition indexing for multiple qubits



