## Present:

### Maintainers and Project Owners

- [ ] Kelvin
- [ ] Damien
- [ ] Slava
- [ ] Adriaan (PO)
- [ ] Jules (PO)

### Community

- [ ] Jordy
- [ ] Adam
- [ ] Diogo
- [ ] Gijs
- [ ] Damaz
- [ ] Vraj
- [ ] Tobias
- [ ] Achmed

## Introduction new member:

## Highlights:

## Breaking changes:
- No breaking changes!

### Quantify-scheduler:
- Present milestones. (https://gitlab.com/quantify-os/quantify-scheduler/-/milestones)

#### Merged MRs (highlights)
- Hotfix for https://gitlab.com/quantify-os/quantify-scheduler/-/issues/260 (temporary) (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/340)

### Quantify-core:
- Please mind we set the deadline for a 1.0 release before the 14th of March.
  - Milestones: https://gitlab.com/quantify-os/quantify-core/-/milestones/9


#### Merged MRs (highlights):
- Temporary hotfix for qcodes 0.32.0 incompatibility. (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/290)
- Minor:
  - fix docs label (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/288)
  - Fix typo in readme (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/289)
  
#### Dataset sprint:

- How do we intend to proceed?
- Do we plan a joint stand-up?
- Build a Kanban board?
- How actual are the action items on the old plan?


## Requested discussions:

- No requests this week

## AOB:

- Planning presentation new device compilation (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/339)
- Introduce Pydantic-based model for data structures (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/341)
- Proposal restructure role of quantum device in experiment flow (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/343)


## Pending topics (for next MMs):

- No pending topics
