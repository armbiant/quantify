## Present:

### Maintainers and Project Owners

- [ ] Kelvin
- [ ] Damien
- [ ] Slava
- [ ] Adriaan (PO)
- [ ] Jules (PO)

### Community

- [ ] Jordy
- [ ] Adam
- [ ] Diogo
- [ ] Gijs
- [ ] Damaz
- [ ] Vraj
- [ ] Tobias
- [ ] Achmed

## Introduction new member:

## Highlights:

- Meetings now on Thursday
- Quantum-circuit to quantum-device layer compilation backend :tada:

## Breaking changes:
* Structure - `Schedule.timing_constraints` has been renamed to `Schedule.schedulables`. It now points to a dictionary of schedulables rather than a list of dicts. (!309)
* Compilation - When specifying multiple timing constraints for a schedulable, the constraint specifying the latest time determines the absolute time of the shedulable (!309)
* Compilation - Deprecated `add_pulse_information_transmon` in favor of `compilation.backends.circuit_to_device.compile_circuit_to_device` (#64, #67, !339).
* Compilation - attempting compilation with missing values in the `DeviceCompilationConfig` configuration will now raise validation errors. Be sure to set initial values when generating a config using the `QuantumDevice` object (!339)
* Compilation - Device compile making use of `.compile_circuit_to_device` no longer modifies the input schedule (#249, !339).
* Operations - The internal behavior of how acquisition channels and acquisition indices are configured in the `Measure` operation has changed slightly. See #262 for details. (!339).


### Quantify-scheduler:
- Present milestones. (https://gitlab.com/quantify-os/quantify-scheduler/-/milestones)

#### Merged MRs (highlights)

- Quantum-circuit to quantum-device layer compilation backend (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/339)
- Schedulable (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/309)
- Correct range motzoi parameter validator (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/351)
- Add generic ICC with default name to instrument coordinator in `__init__` (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/350)
- Qblox backend - add latency correction (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/325)
- Qblox backend - Add clock phase shift operation support (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/346)
  
### Quantify-core:
- Please mind we set the deadline for a 1.0 release before the 14th of March.
  - Milestones: https://gitlab.com/quantify-os/quantify-core/-/milestones/9

#### Merged MRs (highlights):

- Fix qcodes 0.32.0 incompatibility by replacing all references of `qcodes.Instrument._all_instruments` (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/295)
- Absolute deviation as metric for AllXY (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/300)

## Requested discussions:

- Docs failing silently on core (https://quantify-quantify-core.readthedocs-hosted.com/en/latest/api_reference.html)
- Discuss merging process (requested by Adriaan/Edgar)

## AOB:

- Open the floor.

## Pending topics (for next MMs):

- No pending topics
