## Epics (APSMM23 release)

- https://gitlab.com/groups/quantify-os/-/epics/5 Data handling and acquisition flow (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 Formalize hardware configuration (spearheaded by: Edgar)
- https://gitlab.com/groups/quantify-os/-/epics/2 Documentation, User Guide and add New Tutorials (spearheaded by: Daniel)
- Placeholder for parallel work in Quantify that also needs to be done, Edgar to collect

## Highlights

### Breaking changes

- ~`qblox-instruments==0.8` is required for `main` now. Not really breaking Quantify interfaces, but may cause some troubles in the real life for Qblox users.~
Statement is false, there is no qblox-instruments==0.8 release available yet. There will be one in a couple of weeks, and we are already grouping some MRs (after proper review and testing with qblox-instruments dev release) that unlock new features into Quantify into this MR https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/512 (see quantify-scheduler!481)

## Quantify-scheduler

### Merged MRs (highlights)

- Qblox backend minor tuneups (quantify-scheduler!508, quantify-scheduler!499)

## Quantify-core

### Merged MRs (highlights)

- Improving Plotmon stability: update OS environment to disable HDF5 file locking (quantify-core!404, quantify-core!405)
- Create function to find all parents of QCoDeS submodule (quantify-core!401)

## Requested discussions / Any other business

### Subscheduling and classical logic

@dweigand:

> If we have some time, I did prepare some notes on subscheduling and classical logic, which expand quite a bit on epic 3 (classical logic). I will not have time to upload the notes to gitlab today, but can do it tomorrow morning.
This is a long-term thing however, most of my ideas/proposals are for after the march meeting.

```
At first, make it easier to write a schedule by reusing parts
Later, parameterized schedule for use in classical logic

Essentially, write a schedule and allow including it in a schedulable

How to implement: 
- First unrolling step, so we can start using this
- Long term, recognize and execute certain parts directly on hardware

Should be a shared component between backends, shared compilation step/node

Next steps: 
- Daniel to write a prototype to see how complicated the unrolling is
- Depending on that, continue with unrolling or postpone all of it after APSMM
```

## Extra discussion

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

### Formalizing how hardware parameters are updated inside schedules [organized by @dweigand and/or @ddejong]

### Implementing sub-schedules [organized by @dweigand and/or @ddejong]

### Public API (No follow up yet since 2022-08-4) [Owner: @rsokolewicz (guidelines, listing)]
Also see https://quantify-hq.slack.com/archives/C02DE4ZENNQ/p1659630403947179?thread_ts=1659541477.414369&cid=C02DE4ZENNQ

We need to discuss about plans for making formal the public APIs so that we do not expose everything within Quantify
as a public API. This is so that any internal implementations would not break a dependent package. (quantify-scheduler#313)
- What is public (and thus breaking, in need of using deprecation scheme upon change), what is private
- Make whole modules private?

**Decision**:
- Whatever user needs is public.
- Guideline: "private, unless..."
- Process: minor changes incorporated to actual work.

### Line lengths (No follow up yet since 2022-08-4) [Owner: @damazdejong]
Requested by Damaz - A proposal to improve the way we work with Quantify styles.

Damaz: Codacy is complaining when Black fails to reformat a string to 88 chars.
Zhihao: Docstrings are not formatted at all.
**Decision**: Move the discussion online.

### Intermediate representation (No follow up yet since 2022-08-4) [Owner: @gtaifu]
Xiang: we are interested in discussion on intermediate representation, especially in the context of Quingo compiler
(https://dl.acm.org/doi/10.1145/3483528).
**Decision**: Xiang will make slides about Quingo and share them in Slack, later we will schedule a slot for presentation.
