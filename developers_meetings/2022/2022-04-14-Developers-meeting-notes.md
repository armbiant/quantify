
## Highlights

- Michel Vielmetter and Christian Dickel are now developers in quantify-core and quantify-scheduler.

- Quantify-Scheduler brainstorm day (this NL morning):
  - `OUR  IDEAL  HARDWARE  BACKEND.`
  - Prep for future introduction of `feedback` / `classical logic`
  

## Breaking changes

- Release of [quantify-scheduler 0.7.0](https://pypi.org/project/quantify-scheduler/0.7.0/)
   - `Breaking` Support for qblox-instruments driver v0.6.0, see [Qblox ICCs: Interface changes in using qblox instruments v0.6.0](https://gitlab.com/quantify-os/quantify-scheduler/-/wikis/Qblox-ICCs:-Interface-changes-in-using-qblox-instruments-v0.6.0)
     - Required update order Qblox hardware: 
	 1. Firmware (using old driver!) 
	 2. Quantify (updates driver too)
   - New BasicTransmonElement
   - `Breaking` Change for triggers in Zhinst backend


## Quantify-scheduler

- New compilation backend has unwanted clocks bug for qblox backend (_mentioned first in 2022-03-24 meeting notes_) (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/278, https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/371).
  - Hardware testing for fix passes!   
  

#### Merged MRs (highlights)

_None_
   

## Quantify-core

_None_
  

#### Merged MRs (highlights)

_None_
  

## Requested discussions / Any other business


- Carry overs from last DM:
  1. Adopting https://numpy.org/neps/nep-0029-deprecation_policy.html (but don't follow dogmatically if there are good reasons against it)
     - This means: dropping support for Python 3.7
	 - This also means (_to be checked_): we effectively only support Python 3.8
	   - As ZI packages do not officially support above
	   - But, they do run up to 3.10?
	 - **TODO (Maintainers - @slavoutich)**: 
	   - Mention the adopted policy in the contribution guidelines
	   - Adjust pipelines, drop 3.7, include 3.9?
  
  2. Reviewing load:
     - Split "Review me" label to "Review me (Maintainer)" / "Review me (Developer)"?
     - Will continue going over open external MRs in these meetings: aim is reviewing 2 per maintainer until we run out 


## Pending topics (for next DMs)
