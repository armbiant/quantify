People present: Adriaan, Damien, Victor, Thomas, Jordy, Jules

* Victor has joined Qblox in a full-time position as Quantum Application Engineer and will be working on Quantify amongst other things.

* Thomas is trying to unify the scheduler backends. This will give way for a common scheduler "toolbox", which will also allow the pulsar backend to be cleaned-up a bit.

* Adriaan is working on an example lab repo, that will be used by TNO and the IMPAQT project. Ideas that are transferable to the Quantify-core will be merged when it is in a more mature state.
    * Adriaan will reconsider the way of working to prevent 
      a big-bang.


* Core and scheduler release 0.2.0:
    * Has been released.
    * https://gitlab.com/quantify-os/quantify- 
      scheduler/-/merge_requests/43 was not included in the 
      end.

* From now until further notice, Victor is Qblox's interim Quantify maintainer.