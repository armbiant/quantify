# APS March Meeting Abstract 
## Agenda 

- When is the deadline? 
- Should we submit?
- Who are the authors? 
- Who gives talk? 
- What will we give a presentation on? 

## Title 

A professionally maintained open-source quantum control platform. 
A platform for experimental physics 




Scope: 
Quantify-core + Quantify-scheduler. 

- Focus on physics experiments required to characterize,  calibrate, and control quantum computers. 
       - Novel hybrid control model combining gates and pulses. 
       - All the tools required for experimental quantum physics. 
- Robust open-source implementation of concepts of widely used PycQED. 
- Platform agnostic, we show the applicability to a wide variety of quantum systems. 

- Interfaces to higher-level languages to enable quantum algorithms. 


----------------------------------------------------
# First draft by Adriaan
----------------------------------------------------

## An open-source framework for controlling physics experiments

Controlling a quantum computer is an often-underestimated challenge. Specifically, the tune-up, which consists of physics experiments that require low-level access to control parameters and measured signals, as well as programs and data-analysis that are typically defined at a higher level of abstraction. Here, we present Quantify, a robust and extensively documented open-source python-based data acquisition experiment platform inspired by PycQED (Rol et al. 10.5281/zenodo.160327). Quantify contains all the basic functionality to control experiments (e.g., instrument management, live plotting, data storage, etc.), as well as a novel scheduler featuring a unique hybrid control model allowing quantum gate- and pulse-level descriptions to be combined in a clearly defined way. The scheduler is hardware agnostic and has interfaces to higher-level QASM like languages to facilitate easy execution of quantum algorithms.  


----------------------------------------------------
# Second version by Niels
----------------------------------------------------
I'd like to clearly state the problem(s) we aim to solve and think about the claims we can make already right now about this.

## Quantify: an open-source framework for operating quantum computers in the NISQ era

Operating quantum computers in the NISQ era requires scalable and flexible control software and hardware. Where most control architectures are limited to expressing experiments in either series of classical pulses or in dialects of Quantum Assembly (QASM), many algorithms like variational quantum eigensolvers, quantum error correction and gate-tuning experiments require a mixture of quantum gates, parametrically expressed pulses and classical logic. Here, we present Quantify, a robust and extensively documented open-source python-based experiment platform inspired by PycQED (Rol et al. 10.5281/zenodo.160327). Quantify contains all the basic functionality to control experiments (e.g., instrument management, live-plotting, data storage, etc.), as well as a novel scheduler featuring a unique hybrid control model allowing quantum gate- and parametric pulse-level descriptions to be mixed with hardware-executable classical logic. The scheduler is hardware-agnostic and has interfaces to higher-level QASM-like languages to facilitate easy execution of quantum algorithms.

### Funding acknowledgement 
Quantify is maintained by The Quantify consortium consisting of Qblox and Orange Quantum Systems.

#### Authors
M.A. Rol 
C. Attryde
J.C. van Oven 
K. Loh 
J. Gloudemans
V. Negîrneac,
C.C. Bultink


---------------------------------------
# Final version by Niels and Adriaan 
---------------------------------------

## Quantify: An open-source framework for operating quantum computers in the NISQ era
Operating a quantum computer in the NISQ era is an often-underestimated challenge.
Specifically, the tune-up and execution of quantum algorithms, which consist of physics experiments requiring access to control parameters and measured signals, as well as classical logic. Typically, these are defined at a higher level of abstraction and are not supported by current control architectures because they are limited to expressing experiments either as either a series of classical pulses or variants of Quantum Assembly (QASM). Here, we present Quantify, a robust and extensively documented open-source experiment platform inspired by PycQED (Rol et al. 10.5281/zenodo.160327). Quantify contains all the basic functionality to control experiments (e.g., instrument management, live plotting, data storage, etc.), as well as a novel scheduler featuring a unique hybrid control model allowing quantum gate- and pulse-level descriptions to be combined in a clearly defined and hardware-agnostic way. The scheduler allows parameterized expressions and classical logic to make efficient use of hardware backends that support this. This opens up new avenues for efficient execution of calibration routines as well as variational quantum algorithms (VQA).


### Funding acknowledgement
Quantify is maintained by The Quantify consortium consisting of Qblox and Orange Quantum Systems.

### Authors
- M.A. Rol
- C. Attryde
- J.C. van Oven
- K. Loh
- J. Gloudemans
- V. Negîrneac,
- T. Last
- C.C. Bultink
