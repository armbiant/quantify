People present: Adriaan, Kelvin, Damien, Thomas, Jordy

Core and scheduler release 0.2.0:
- Quantify release 0.2.0 ready.
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/43 will be included in 0.2.0 by Damien.
- Names changes regarding NCO/IF etc. will be included in release 0.2.1.

Scheduler:
- Scheduler documentation needs to be improved. Qblox and Orange Quantum Systems maintainers should create appropriate issues for this.