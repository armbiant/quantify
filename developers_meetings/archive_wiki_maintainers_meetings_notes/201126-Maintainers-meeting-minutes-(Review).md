Today's meeting is the Sprint Retrospective/Planning.

Restrospective:
 - Callum explained what pain points are.
 - Assigning of issues could potentially lead to silo-ing of specialization.
 - Victor asked if we have a plan on how to get the code running in Windows.
   - USB through docker, pycqed might be tricky?
 - Issue is clients' operating system is never fully clean.
 - Need to address code environment (OS, packages, docker...) issue in the future.

Planning:
 - How much work for quantify-core and quantify-scheduler?
 - Core: 4 key features not refined yet.
 - Scheduler: Resource-refactor issue is blocking, except for design work. Adriaan still working on it, and has a documentation which explains the new concepts within scheduler. Jules to assist in fixing the tests. Order of time to completion is approximately in days. Estimated done before end of this sprint [1 week left]. A sprint is 2 weeks. Now we're midway.
 - After the sprint, it is recommended to push new releases for both modules.
 - Release: Merge to master from develop. See the MR !55 for what needs to be done. Release notes to be aided by Kelvin.
 - Adriaan made comment to core that it is mature enough for even kQubit initiative. Still sufficient to have a dirty implementation through the scheduler for simple experiments.
 - Analysis groups: 
   - Affects data storage and handling.
   - Better to be separate object.
   - dataset, multiple x and y values.
   - snapshot -> parameters and last updated time
   - Typically contains: 
        - fitting results (json structure)
        - Quantities of interest (json structure)
   - Explains the approach to have the dirty approach for experiments to identify potential common variables/structures to be abstracted away for analysis groups in core.

