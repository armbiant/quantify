This is a technical meeting to discuss how to implement measurements in Quantify-scheduler.

This meeting should cover at least

### Translating quantum circuit level /gate level to the pulse level/device level.

  * That is done using configuration file
  * A measurement will be compiled to a combination of control pulses and acquisitions embeded in the same operation.
  * From a gate level a measurement should return a bool and the qubit state, cant define right now because of unclarities.
  * Concept aqc protocols
    * Measurement protocol will be renamed to acquisition protocol.
      * Has to do with the return types on data levels
  * The definition of the aqc protocol should be in the configuration file instead of the compilation.py
  * Should the user change the config file to change the aqc protocol? Yes, but. If you want to change the waveforms without changing the program. This requires iteration on how to design this. In the future the use should be able to provide these parameters at the gate level instead of the pulse level.

TODO:
  * Create new issue to redesign the config file to include pulse type, aqc types and its parameters.
  * Jules will modify the prototype.

### Provide a list of different “Acquisition protocols” that are to be documented and described later.
* A description of what processing steps are done on a signal
* What the return type is.

  * Added description to [#36](https://gitlab.com/quantify-os/quantify-scheduler/-/issues/36) of aqc protocols including:
      * What do they do
      * How do they look like
        * Types
        * Return types

### Data registers
* Where do the return values of an acquisition get stored? 
* How do we return them to the user?
* How do we deal with “binning” of data? How is this represented in the schedule?

  * Where does the data go?
    * To be defined.
  * What is the processing we do on the data?
    * To be defined.
  * Default behaviour of the data register is a list which will be appended with each new value.

### Program flow, how do we deal with gettables, settables, schedules and controlling the different instruments (control station instrument?).

I think it also makes a lot of sense to discuss the “device_config.json” and how we (should) include measurements in it.

Currently the discussion is spread out over the following locations:

* [!51](https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/51) by Jules https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/51
    - Contains two “Measurement Protocols”
* [#36](https://gitlab.com/quantify-os/quantify-scheduler/-/issues/36) https://gitlab.com/quantify-os/quantify-scheduler/-/issues/36
    - Contains a lot of discussion
* [!2](https://gitlab.com/quantify-os/quantum-control-design/-/merge_requests/2) of the Quantum Control Design repo https://gitlab.com/quantify-os/quantum-control-design/-/merge_requests/2
    - Contains a proposal for terminology (including “Measurement Protocol” and the formula for weighted integration)