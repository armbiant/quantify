Quantify meeting
October 15st , 2020
 
Present: Jules, Jordy, Victor, Adriaan, Callum<br /> 
 
Quantify consortium agreement signed<br /> 
The QCB assigns the following maintainers: Adriaan Rol (OQS), Callum Attryde (Qblox), Victor Negirneac (Qblox),  Jules van Oven (Qblox).<br /> 
Project management.<br /> 
We use GitLab to track issues and tasks. We will document minutes of meetings in wiki notes. We would like to work using Agile framework. We will discuss in Slack how to get there and who does what.<br /> 
Open Repos and Fix Documentation<br /> 
            Aad is fixing this (see point 4).<br /> 
Single top Quantify repo<br /> 
We stick to the plan of having multiple repos. Adriaan will take the lead in this and ask for help if needed. This will be done before the next meeting.<br /> 
Resources<br /> 
Jules will write down feedback and share on Slack. Jules proposes to converge in the coming weeks<br /> 
Website<br /> 
Read-the-docs for toplevel repo (Adriaan)<br /> 
Other<br /> 
    Add this to wiki. This will be the last google docs.<br /> 
    Hackathon: We need formal invite (Callum).<br /> 
