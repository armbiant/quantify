## Agenda 

* Misc releases. 
* Discuss issue board/project progress. 
     * Quantify-core. 
     * Quantify-scheduler. 
* Any other business 

### Misc releases 
- Periodic releases of whatever has been merged into develop. 
- Strive once every 2-4 weeks. 
- The decision for a misc release will be made by the release manager(s). 
- Additional misc releases when a hotfix is needed. 

Why? 
- Way to deliver smaller updates without requiring multiple months between milestone releases. 
- Show that the project is actively being developed. 
- Release is available on PyPI and clearly tagged. 

Adriaan will be the release manager. 

### Discuss project progress. 
#### Quantify-Core
#### Quantify-Scheduler

We have identified a shortage in the bandwidth of the developers that is limiting progress. 

This needs to be addressed by @jvanoven and @adriaanrol. 

Issues still need to be coupled to milestones. @adriaanrol will take care of this. 

### Other business 

