Present:

## Current state (Max 15 mins):

### Quantify-core:
* Fair to say we're not going to make the deadline. What is realistic? Next week?
* Documentation for analysis framework [quantify-core!155]. Conflicts with quantify-core!156.

Issues left:
quantify-core#184
quantify-core#183
quantify-core#181
quantify-core#180
quantify-core#177
quantify-core#172
quantify-core#165
quantify-core#152
quantify-core#136

  - Some items can be pushed to 0.4.1.
  - Documentation key (quantify-core#177).
  - quantify-core#172 not key.
  - Key issues to be done by next week.


### Quantify-scheduler:
* MR quantify-scheduler!81.
* quantify-scheduler!77 and quantify-scheduler!87 are ready afaik.

Issues/MRs left:
* quantify-scheduler#44
quantify-scheduler#48
quantify-scheduler#101
quantify-scheduler#102


## Discussions:

### Core release 0.4.0 (max 15 mins):
* Issue quantify-core#180, do we need to redefine the issue? No longer just pyfakefs, but more of the intended behaviour for tests using pytest (i.e. tests should only test single point of failure).
  - Unit tests should test for only one behaviour
  - Unit tests should not write to memory
  - Could create temporary directory
  - Copy test data directory to tempfile tempdir 

### Scheduler release 0.3.0 (max 15 mins):
* quantify-scheduler#44 (Kelvin yet to check with Damien on the qblox backend)
* is quantify-scheduler!83 for 0.3.x or 0.4.x?
* Who can and wants to pick up quantify-scheduler#48, quantify-scheduler#102, quantify-scheduler#101?


## AOB:
* https://unitaryfund.github.io/unitaryhack/
  - Parser from quantum circuit to quantum gate level
  - Pulse level visualisation
  - Parser for IBM qiskit
  - Multidimnesional visualization
* Request to assign yourself in the gitlab issues whenever you're working on it. Else it is difficult to know if an issue is currently being worked on.