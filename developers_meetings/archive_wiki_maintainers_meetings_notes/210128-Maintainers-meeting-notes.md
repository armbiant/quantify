People present: Adriaan, Damien, Victor, Thomas, Jordy, Jules, Kelvin

# Agenda

Departure from strict process of sprints while we indeed still keep the spirit of it. Kelvin wants more technical discussions and learnings during those discussions.

* State so far:

    * No huge bugs except for python 3.9 failing due to pyhdf dependency. #132
    * Core 0.3.0, and Scheduler 0.2.0 at a working state for users to be able to be productive.
    * New MR from one of the uses. AnalysisClass have active discussion (will be visited as a point of discussion). Once we merge !63, we can make a version bump to 0.4.0
    * We can also bump Scheduler to 0.3.0 once we get zhinst backend merged.
    * We have managed to engage another potential user. Pieter from TNO. Thanks to the talks between Adriaan and all of you during the sequencer meetings and keeping quantify on their radar.


* Infrastructure design choice of AnalysisClass

    * Issues can be created from MR63
    * Inventorize features we need to incorporate in the general framework


* Point of discussion: Use black for PEP8 style [Helps code review process]


# Discussion:

- Not focusing too much on process today
- No big bugs, except CI fails on python 3.9 (Victor tacking this one)
    - Seemed critical but has not been fixed
    - We were lucky no one used it yet, we agree it must be fixed

## Scheduler

- Version to be bumped when ZI backend MR is merged
- Some utilities that Thomas worked on are going to be useful also for Qblox and other
    - Likely worth a minor release
- Jordy suggested to wait for Qblox backend so that both would be harmonized
    - Adriaan: that can also go in a misc release if takes to long
- Regarding the work Thomas is doing:
    - Adriaan:
        - Introduction of dataclasses-json
        - Introduction of utility methods

## Analysis framework

- targeted for 0.4 Quantify
- great to have this WIP MR
- there are potential user already
- allows for timely feedback

## Analysis Class MR

- Adriaan making a distinction on the topics present in this MR:
    - Abstract topics related mostly to the main base class, e.g. flow of analysis, way of saving quantities, etc.
    - Particular subclasses of analysis
        - What specific analysis to make part of the quantify-core? --> Inventory/new issues
        - Creating examples of some of these particular analysis

- To do before the MR:
    - quantities of interest (Victor to work on this):
        - interface
        - saving to disk
        - accessed by the user

- Non-negotiable particular analysis to have in core
    - We created issue for this #136
    - Inspiration from PycQED

## Code style

- Suggestion: use black auto-formatter
- New issue was created at the quantify project level
    - tasks were defined
- We agreed to move to black and never think of it again (Victor picking this up)
    - Victor will ask for help Thomas, Kelvin, Adriaan if needed
    - For external contributions the maintainers will take care of reformatting the code