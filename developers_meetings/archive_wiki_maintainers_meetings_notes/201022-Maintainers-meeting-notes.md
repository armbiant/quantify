# Quantify meeting October 21st, 2020

Present: Jules, Jordy, Victor, Adriaan, Callum , Kelvin 

## Agenda 
* Adding Kelvin to maintainers 
* Hackathon 
* Update on Qphox and TNO (launching test partners) 
* Technical discussions
    * Documentation and opening of repos 
    * Scheduler resources (new style)
* Project governance (Agile) 
    * Discuss way of working 
    * Review issues? 
* Other points

## Adding Kelvin to maintainers 

* Kelvin is added to the Quantify maintainers 

## Hackathon 

- Everyone is able to run the test notebook. 
- Goal of the hackathon: Implement loops in the scheduler. 
- Starts at 14:00, fully remote, will keep video conference open all the time. 

## Update on Qphox and TNO (launching test partners of Quantify) 

- Both organizations have expressed desire to use Quantify.
- TNO 
    - If we can create two examples from the spin-library then they can verify working and give stamp of approval. (Important for getting TNO budget to work on this). 
        - Charge stability diagram. 
        - Time-Rabi 
        - Will be verified on a scope. 
    - Interested in selling their contributions to us. 
- Qphox 
    - Single qubit characterization 
    - Resonator spectroscopy using Qblox hardware and Quantify software.
    - Time-domain experiment (Rabi) ultimate goal. 

## Technical discussions 

### Documentation and opening of repositories 

- Both quantify-core and quantify-scheduler are now open. 
   - PyPI, ReadTheDocs and GitLab 

Installation issues, still not as easy as `pip install quantify`. Should be improved. 
Kelvin will file issue and make suggestion on how to improve. 

### Scheduler resources new style 

Demo by Jules 

- Scheduler up to backend has no notion of Qblox equipment 
    - Resources remaining are "ports" and "clocks" 
- Mapping file created that describes how pulsars are connected to ports. 
- mapping file part of backend. 


#### Circuit diagram 
![image](uploads/b13fbea6e627bed2e867dfb88a55a9cd/image.png)

Some minor bugs in visualization. 

#### Configuration file 
![image](uploads/7960e5c6ad140f691d2f741e00efac5b/image.png)

#### pulse visualization

![image](uploads/101a03d14a8778697639721489b8a02c/image.png)

![image](uploads/da6e194d467b1891b64f077aaf43746f/image.png)

Pulse visualization

![image](uploads/6895f74f7e4702c476f4ccd26f43feac/image.png)

Can set either lo_frequency or nco_frequency. 
Specified in pulsar config, to be discussed if this is the most sensible place. 


Demo looks great! 
This is a suitable prototype of progress made in formalizing the layers. 
Cleanup and documentation required before it can be merged into the develop branch. 
- Jules will write this documentation 
- Adriaan will help Jules in getting the documentation to work properly locally. 
- Create an issue for this to adhere to Agile methodology. 

### Project governance 

- Callum gave a presentation on Agile. 
- Jordy: need to start working in sprint form. 
- Starts with defining milestone. 
- Adriaan: propose use weekly maintainers meeting as a bi-weekly sprint meeting
- Kelvin: 1 hr not enough for proper sprint meeting. 
- Callum  two different things 
    - Sprint review -> you go over all points to verify if all issues have really been addresssed
    - Sprint retrospective -> review process 
    - Sprint planning meeting -> plan milestones/releases 

Jules: what is a light way version of Agile 
- We want sprints 
- We need a release date (v1.0) 1st of Feb? 
- Set of deliverables for this date (key features of milestone). 
- People in this call. determine what features need worked on . 

#### Summary 

- Jules and Adriaan (as project board) will come up with milestone and key features. Including input from all maintainers. 
- Weekly maintainers meeting, sprint meetings alternated by "other-stuff" meeting. 
- In addition technical meetings on call by whoever is required. 

Deliverable next week: 
- First proposal milestone v1.0 

Decided we will have a separate Slack workspace for Quantify. 

Next meeting: 
- Review issues
- First "sprint" meeting







