Current state (20 mins):

    Quantify-core:
    * Codacy integration for the pipeline. !108

This is done. 

    * Merged advanced batched control mode. There's a new dataset format too. !98

Adriaan is considering to go full xarray with the dataformat.

    * Show usage of xarray features. #157, #156

This will be addressed by Adriaan.

    * Concerned about global Parameters being used in the tests? #155

Victor found this and wants to notify us about this. Please try to prevent this.

    * After polishing the analysis class (!89, !115), we can have quantify-core 0.4.0

Latest documentation failed. This should be fixed in before the next release.

    Quantify-scheduler:
    * Codacy integration for the pipeline. !53
    * Bug fixes for the set_datadir test in windows system? !54
    * Thomas to elaborate on the zhinst backend update?

Thomas shows some figure about his work on the ZI backend. Next is the integration of the raw-trace.

To be discussed (30 mins):

    Quantify-scheduler:
    * MeasurementProtocols. !51 is too big imho. Continuation of discussions.

Adriaan explains that the merge request only involves two points and that the other points are more for context.

Adriaan explains our new insights on channels and acquisition_index. Thomas raises his concerns about the scalability. Adriaan thinks it is scalable. 

    * Demo for quantum control stack object. #66

Damien showed a small demo. People liked it in general but we need to discuss the details further. He also showed 1d and 2d spectroscopy using the scheduler.

For next week (10 mins):
    
    * We will be talking about processes next week after we've reached 0.3.0 of quantify-scheduler. Get ready all your pet peeves and send them over to me.