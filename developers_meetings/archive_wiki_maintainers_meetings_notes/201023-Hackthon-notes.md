# Hackathon for adding classical logic 


## Jules 

- Schedule concept not touched.
- Program is a combination of schedules.
- Subroutine is a parameterized schedule. 
- Schedules can be parametrized.


![image](uploads/a13ee017c61617064fe9bdd872be0503/image.png)


![image](uploads/6e7d7473d13948bc5812e4f6a27aa73e/image.png)

amp and duration set to string values. 

![image](uploads/658698003226baf3fc0bc05dcd048437/image.png)

## Jordy 

Idea: 
- Added two logical operations to logical operation library 
`for_loop` ad `for_loop_end` that go in the schedule. These are "operations?" 
Iterator list is argument. 

Refer to the amp iterator in the for-loop to determine what value to take. 
Backend and visualization tools should deal with it. 

You get a schedule where you don't know the duration of the schedule anymore. 

![image](uploads/a214654e9a3bb8484163ff806d9e3208/image.png)

## Kelvin 

Based on Sander's example. 
Like to keep function available so that it becomes a user-defined function (UDF). 
Add insert loop with user defined function 
![image](uploads/41aa77e3bcb8ef8b601d2b3ff735fa4a/image.png)

Create unrolled version compiles (replacement of the for-loop). 

Runs up to assembly. 

## Callum 

Changed current schedule to allow enter and exit. 
Added simple repeat. 

Create sub-schedules which are used as normal. 
Add extra bookkeeping info on how many loops 

![image](uploads/02dd436532a6880519f163589248253f/image.png)

![image](uploads/602876a5e81b3825937807f4d146835b/image.png)

Messed with graphs to mark when loops are occurring (vertical bars) 

![image](uploads/1f141199392d7b1528721fa011a0e0b4/image.png)


## Adriaan 

Idea : focus on the data structure. 
- Schedules can be nested and added to schedules as if they are operations 
- New attributes added to a schedule 

![image](uploads/0c73ab88f222fbb4b87b073584727539/image.png)

![image](uploads/75d4a8942964c8118134649c676b7b05/image.png)

## Victor 

Reference dictionary of a pulse, want to modify it parametrically. 

Besides counter, hard-code duration of the sub-loop. 
(channel added because hacking). 

![image](uploads/3f156f66ee65426962b42b081f9a2c9b/image.png)

Collection of pulse 

Loop is a kind of sub-schedule. Very similar to other concepts

![image](uploads/021ad649d30e75862f1fb37cfc25f914/image.png)

![image](uploads/3078603e7618150042595b395dc1ebe9/image.png)


# Recap/summary 

## Content 
- Nested schedules were used with different names.
- Context managers (with) were used to provide "nice" syntax for users. 
- Logical operations were added directly to the top-level schedules (different from context manager approach). 
- Instructions don't have a good variable name. 
- Instructions + arguments were associated with (nested) schedules in different ways. 
- Callum included the loop info in the visualization. 

## Process 
- This was a very successful hackathon! 
- Similarities in approaches and also different approaches. 
- Everyone had working examples!

Hackathon code (including notebooks) will be committed to individual Hackathon branches in the scheduler repo. 