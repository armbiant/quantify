Present:

## Current state (Max 30 mins):

### Quantify-core:

* Inspect utility is available which extends the functionality of the python inspect module.
* Changing namespace is ready.
  - Next Friday deadline for change

### Quantify-scheduler:

* ZI related MRs almost all merged (except for mixer corrections).
* Status on Qblox backend?
* Changing namespace is also ready for testing.

## Requested discussions:

* Decide design solution: [quantify-core#215](/quantify-os/quantify-core/-/issues/215)
  - Clash between .name and .attrs['name'] in xarray datasets
  - Postpone discussion for anouther meeting
* Define smaller milestones for both repos.
* \[Request by Niels\] Change `ControlStack` name because it is clashing with the equivalent concept of physical instruments used to control a stack.
  - Jules to create issue with proposals
* \[Requested by Victor\] Pulses units, voltage vs power, where we specify the pulse and what about the frequency-dependent response of a line?
  - Signal as a function of time should be volts
  - Gain can be dB
* \[Requested by Kelvin\] Testing infrastructure still very fragile. We need to be able to peg a commit number of quantify-core with that of scheduler instead of waiting for an official release of quantify-core.
  - Can use latest version in exceptional cases
  - Victor/Kelvin to fix offline
* Decide hard time/deadline on when we want to change the namespace.
  - Next Friday

## AOB (max 5 mins):

* TBD