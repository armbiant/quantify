Present:

## Current state (Max 30 mins):

### Quantify-core:

* Docker Unix containers are ready.
  - Do demo as soon as possible
  - Docker testing environment still in progress

### Quantify-scheduler:

* ZI related MRs almost all merged.
* Qblox ControlStack is merged. quantify-scheduler!112
* Artificial detuning in ramsey schedule merged. quantify-scheduler!120

## Requested discussions:

* Stale MRs and issues: (Definition of Quantify dataset) quantify-core#187, quantify-core#139, quantify-core!111
* \[Request by Niels\] Change `ControlStack` name because it is clashing with the equivalent concept of physical instruments used to control a stack.
* `ControlStack` needs to be an Instrument instead of Station. quantify-scheduler!139
  - Need a string reference to control stack, not possible with station
  - Resolve to make this an instrument
* Docker testing environment still in progress.

## AOB (max 5 mins):

* Change namespace tomorrow
* Discussion on measurement function and device element design
  - String reference to control stack in transmon element
  - Pass in device element and qubit names to measurement function

* TBD