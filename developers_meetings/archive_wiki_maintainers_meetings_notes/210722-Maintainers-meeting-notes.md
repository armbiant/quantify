Present: Damien, Victor, Jordy, Diogo, Jules, Adriaan

## Current state (Max 30 mins):

### Quantify-core:

* Video mode MR submitted

### Quantify-scheduler:

* [quantify-scheduler!162](https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/162)
* Link between IC and MeasurementControl is missing?
* Next release
  * We want to release, but we are postponing the decision, probably for next week, because don't have a clear list of what we need/can forego for this next release

## Requested discussions:

* Time for new releases. Especially with the namespace change in develop.
* Should we open up our maintainers meeting to the public as recommended by UF?
  - Both parties agreed to open up the meeting
  - Advertise on the Quantify Slack group and Unitary Fund discord (possibly others) that we have open maintainers' meetings every Thursday at 4pm.
  - Change platform from Teams to Google Meet/Discord

## AOB (max 5 mins):

--