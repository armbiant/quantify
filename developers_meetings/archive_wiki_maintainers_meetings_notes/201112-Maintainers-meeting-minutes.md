## Agenda 

* Discuss issue board/project progress. 
     * Quantify-core. 
     * Quantify-scheduler. 
* Refactor of scheduler repo.
* Naming of `soft` and `hard` experiments. 
* Version numbering. 
* Best practices documentation

### Issue board discussion 

- Discussion on the process of issue management. 
     - Issue board should not be discussed in this meeting, someone should take charge of this during the week. 
     - Issues can be flagged for discussion in maintainers meeting during working hours. (Maintainers propose discussion points). 
     - Callum will be the scrum-master from now on. He will ensure the issue board is kept up to date and manage priorities. 
     - Callum will provide a proposal of the workflow and responsibilities of the scrum master. 
     - Adriaan will add a label for discussion needed. 

### Refactor of Quantify-scheduler. 
Likely breaking. 
Should a MR be created? 
Two things Jules can do. 
- Run tests (needs help).
- Update documentation (needs help). 

- Develop should be a pristine branch (should always work). 
- Don't want to add dirty stuff to develop. 
- Approach, Jules will create put his changes in a merge request. From there we will iterate on the branch. 

- Order of important/blocking issues in quantify-scheduler. 


### Version numbering
- Synchronizing version numbering of core and scheduler? 
     - Better for user (confusion) if each component has the same version number. 
     - Introduces overhead to deal with (difficult), let's not do it. 
     - Decision: for now we don't keep versions in sync. If issue arises, we will reconsider. 

### Soft and hard experiments. 

Gettables and Settables have a `.soft` attribute that is optional. 
Key defining factor for being "soft" or "hard" is the data shape, is it "array-like". However, array-like is ill-defined. "batched" describes the relevant mechanism and implies the data shape. 
`.is_batched` is the best proposal we have. 

Users in general struggle to understand the Gettable Settable concept (Victor, Kelvin and Jules all experienced this). We believe this change should alleviate this pain. 

We have voted unanimously to accept this change. Issue will be updated with this reasoning. 


### Best practices documentation. 
A lot of the design ideas on how to use things like .prepare and .finish are not clear. It appears these also relate to data logging. 

It is clear that not only is it not clear how these should be used. We are also not even internally agreeing on how the data logging should work. This confusion results in unclear communication to the users (documentation). We will return to this agenda point next week. 


