## Present:

### Maintainers and Project Owners

- [ ] Kelvin
- [ ] Luis
- [x] Damien
- [x] Victor
- [x] Slava
- [x] Adriaan (PO)
- [x] Jules (PO)

### Community

- [ ] Jordy
- [ ] Adam
- [x] Diogo

## Current state:

- Pipelines fixed.
- Link to Quantify Slack published (repos header, README header, CONTRIBUTION, issues template).
- FYI: Biweekly Marketing meeting established
    - [teams: general, tech, Unique value propositions (USPs) & logo, comm. channels].
    - Tech team: Victor and Adriaan.
        - Action items / responsibilities:
            - Define policies for releases, versioning, deprecation and related.
            - CI builds and releases.
            - Defining features to ship in Quantify-core v1.0

### Quantify-scheduler:

- MERGED: Huge MR (quantify-scheduler!180) on gettable/binning/averaging/binning+append.
    - 🥳🥳🥵🥵🥵🥵
    - Not all of it is pretty and we know it...
    - Missing compilations layer(s) identified.
        - Adriaan working on defining the layers. [WIP figure](https://quantify-hq.slack.com/archives/C02DE4ZENNQ/p1631179193033800).
    - Lots of issue spawned.

### Quantify-core:

- Focus for the near future: releasing **Quantify-core v1.0**.
    - Timeline target: December.
    - High-level features target: Implementing and integrating the new dataset.

## Requested discussions:

- (Proposal by Victor) move the meeting notes to the `quantify-os/quantify`. MR created !1.
    - Why?
        - Edit locally.
        - Version control.
        - Simple to edit, better online editor.
        - Easy to find.
    - Agreed? Yes.

- (Requested by Adriaan) Suppress warning in load_settings_onto_instrument when trying to set parameters to None (quantify-core#232).
    - When loading parameters from snapshots, sometimes parameters are not yet configured and are set to None. When reloading settings, we get warning due to setting to None.
    - We agree that these warnings should be suppressed.

- (Requested by Damien) Suggestion for restructure of pulse/acquisition libraries (quantify-scheduler#159)
    - Dataclass makes structure more clearly defined than dict
    - Issues with validation and serialization
    - Issue marked as key and assigned to Slava

- (Requested by Damien) Qblox control stack component produces exception when instrument is not used in experiment (quantify-scheduler#140)
    - A workaround has been implemented
    - Related issue: sync enable issue

- (Requested by Victor) Heads up: we are not following proper process for milestones/issue triage/features implementation/etc.
    - Review this when all maintainers are back, and get back on track.

## AOB (max 5 mins):

- Update analysis classes for calibration points
    - Add as and optional argument

- Reorganise the analysis code files into more logical structure
    - Organise into different files per category e.g. single_qubit_timedomain

- May be better for core and scheduler to be one repo
    - Easier to merge, deal with dependencies
    - Alternatively, reduce coupling between core and scheduler
    - Discuss another time

- (Requested by Slava) rename/restructure branches
    - Suggest combining master and develop into main
    - Maintenance branches as well
    - Decision approved, create issue to follow up later

## Pending topics (for next MMs):

- (Suggested by Adriaan) nightly releases
- (Requested by Slava) use our own CI fleet (custom runners)
- (Requested by Victor) pending implementation: ship tests with pypi
    - Kelvin did you pick this up a while ago?
