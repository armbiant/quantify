## Present:

### Maintainers and Project Owners

- [X] Kelvin
- [ ] Luis
- [X] Damien
- [X] Slava
- [X] Adriaan (PO)
- [X] Jules (PO)

### Community

- [X] Jordy
- [ ] Adam
- [X] Diogo
- [ ] Gijs
- [X] Damaz
- [X] Vraj

## Introduction new member:

## Highlights:

### Quantify-scheduler:
- Present milestones. (https://gitlab.com/quantify-os/quantify-scheduler/-/milestones/3)
  - Multiplexed readout is now possible. This is a breaking change if you use single channel gettable. A merge request with deprecation will be pushed by Slava. Deprecation policy should discussed.

- Deprecation policy\
  Slava proposes that every deprecation should be at version 1.0. Major breaking changes should be bumped to major version. Minor breaking changes should be bumped to 3 minor versions ahead. Same policy as numpy. Minor and major breaking should be determined by maintainers.
  Why not already start using policy(Damaz)? Delete deprecated objects already at 0.9. Untill version 1.0, a final version is not excpected.
  The deprecation policy will start now. In the merge request Slava is making, details can be discussed further.
- MR that are stale will be deleted by Kelvin and Damien.

- Multiple acquisitions for schedule gettable in progress. Verify with Adam to see if Analysis will be a problem (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/299). Still pending.
- Tutorials key: (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/241)
- Are these still issues?
  - https://gitlab.com/quantify-os/quantify-scheduler/-/issues/240
  - https://gitlab.com/quantify-os/quantify-scheduler/-/issues/243

#### Merged MRs (highlights)


### Quantify-core:
- Realistically delayed to mid next year (https://gitlab.com/quantify-os/quantify-core/-/milestones/9)\
  - #254 more-or-less done
  - Start on #259 and #255
  - Slava to do #255, Kelvin #259
- Version 1.0 release
  - Dataset 2.0 is still blocking. Slava is only one working on it.
  - March meeting (14th March) is the deadline. February should be used to finalize the version.
  - Core features should be stable. The website and visual stuff should look good also.
    - Bug in the live plotting. Warning for every datapoints are thrown in the console. Is not catched by tests. 
  - Timings between top level and assembly are not the same. Should give warning if the timings in top level can't be produced by hardware. Damien is on top of this.

#### Merged MRs (highlights):

## Requested discussions:

## AOB:
- Last dev meeting of the year!
- Resume on 7th Jan. (Damien to chair and prep)

## Pending topics (for next MMs):
