## Present:

### Maintainers and Project Owners

- [x] Kelvin
- [x] Luis
- [x] Damien
- [x] Victor
- [x] Slava
- [x] Adriaan (PO)
- [x] Jules (PO)

### Community

- [x] Jordy
- [ ] Adam
- [ ] Diogo
- [ ] Gijs
- [ ] Damaz

## Introduction new member:

## Highlights:
- We now have both v0.5 for core and scheduler! Thanks to Damien and Adriaan for the release in quantify-scheduler.

- Quantify-core dataset v2 specification has been merged! (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/224).


### Quantify-scheduler:

### v0.6 milestone
- We need to come up with prio list for v0.6. Kelvin and Damien to discuss and come up with initial `key` prios from the issues list?

- Breaking changes: `instrument_coordinator.last_schedule` is now a property (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/252).

- Factor out `mock_setup` from `test`? (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/211)

- RampPulse incorrect sampling (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/212)

- Default `init_duration` for spectroscopy schedules has been reduced from `200e-6` to `10e-6` (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/237/)

- ICC for generic components (https://gitlab.com/quantify-os/quantify-scheduler/-/issues/208, https://gitlab.com/quantify-os/quantify-scheduler/-/issues/215).
    - Kelvin to implement it.

- Refactoring of operations etc. To be an issue raised by Adriaan.
    - Kelvin to do the refactoring.


#### Merged MRs (highlights)
- ScheduleGettableSingleChannel underwent a minor refactor (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/249).

- ScheduleGettableSingleChannel now supports trace acquisitions (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/248).


### Quantify-core:

- Mentioned in highlights.

#### v0.6 milestone

- Milestone board: https://gitlab.com/quantify-os/quantify-core/-/milestones/9
- Target: End of October
    - Would be missing it but Damien and Kelvin to discuss an achievable release date.

- Discussion (still draft):
    - Dataset v2 implementation draft (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/243)
    - Multifile data extractor function (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/239)

- Discussion:
    - Improvements proposal ci (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/215)
        - Suggestion to have it in codacy and only check for the differences so far.


#### Merged MRs (highlights)
- Plotmon refresh has been factored out of MeasurementControl (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/248).
    - Same needs to be done for Instrument Monitor following similar implementation.


## Requested discussions:

- (Requested by Kelvin) Have formal procedure on external contributions.
    - Kelvin to create a section for Maintainer's guidelines and workflow to formally have such procedures in quantify-core.


## AOB:
-

## Pending topics (for next MMs):
-
