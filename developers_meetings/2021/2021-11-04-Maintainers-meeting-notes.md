## Present:

### Maintainers and Project Owners

- [x] Kelvin
- [x] Luis
- [ ] Damien
- [ ] Victor
- [x] Slava
- [x] Adriaan (PO)
- [x] Jules (PO)

### Community

- [x] Jordy
- [x] Adam
- [ ] Diogo
- [ ] Gijs
- [x] Damaz

## Introduction new member:

## Highlights:
- New logo for Quantify!
- Quantify-core v0.5.1 has been released. (https://gitlab.com/quantify-os/quantify-core/-/releases/0.5.1)
- Pre-commit hooks for both packages. (Affects commit and push procedures)
    - Clickable link
- Minor refactor of Quantify-scheduler. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/256)
    - No more `quantify_scheduler.types`.


### Quantify-scheduler:
- Present milestones. (https://gitlab.com/quantify-os/quantify-scheduler/-/milestones)
    - Solution to timing issues, discuss in design meeting
    - Parameterisation of pulses at runtime? Do before classical flow control.
- More work towards the ZI backend. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/263)
    - Docs failing
- Qblox ICC has a lazy set feature. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/230)

#### Merged MRs (highlights)
- Removed redundant determine_absolute_timing command in def qcompile. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/259)
- Ramp pulse sampling for fixed. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/258)
- We can set an output port for the `heterodyne_spectroscopy` schedule. (https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/262)


### Quantify-core:
- v0.6.0 changed to v1.0 instead due to capacity issues. (https://gitlab.com/quantify-os/quantify-core/-/milestones/9)
- Discussion to clarify naming. (https://gitlab.com/quantify-os/quantify-core/-/issues/224)
    - Go ahead with instr_ prepend

- To be merged? Dataset v2 implementation. (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/243)
    - Assign to Slava


#### Merged MRs (highlights)
- Qcodes parameters for docstring no longer need an explicit docstring key. (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/255)
- We now use Prospector as way to get mypy into codacy. (https://gitlab.com/quantify-os/quantify-core/-/merge_requests/259)


## Requested discussions:


## AOB:
-

## Pending topics (for next MMs):
-
