## Present:

### Maintainers and Project Owners

- [x] Kelvin
- [x] Luis
- [x] Damien
- [x] Victor
- [x] Slava
- [x] Adriaan (PO)
- [ ] Jules (PO)

### Community

- [x] Jordy
- [x] Adam
- [ ] Diogo
- [x] Gijs Vermariën (OQS intern, welcome!)

## Introduction new member:
- Gijs (from Orange QS)

## Current state:
- N/A.

### Quantify-scheduler:

- N/A.

### Quantify-core:

- Focus for the near future: releasing **Quantify-core v1.0**.
    - Timeline target: December.
    - High-level features target: Implementing and integrating the new dataset.
    - See discussion in slack and gitlab [quantify-core MR 224](https://gitlab.com/quantify-os/quantify-core/-/merge_requests/224), [quantify-core RTD MR 224](https://quantify-quantify-core--224.com.readthedocs.build/en/224/technical_notes/index.html).
    - Action: New meeting with interested parties
    - Action: How do we divide the work units such that we can release the versions in a timely manner?
    - Action: Define the specification [Victor already working on]
    - Action: See example from Sander de Snoo [modularization]
    - Action: Update InstrumentMonitor and plotmon to use the data server kind of IO.
    - Action: Analysis, MC
    - (From Slava) Hardcoding `dim_{i}` might be a bad idea.
        - Victor and Slava will chat about this.
    - (From Slava) The advanced Surface-7 example might have issues as well.
        - To be discussed at a later point in the future since the current spec is intended mostly for simpler use-cases, while that examples was a proof of feasibility.

    - (From Adriaan) We can already chop the big task down into multiple smaller issues and making the code more modular:
        - Specification
            - Depends on: figure out the `dim_{i}` first.
        - MeasurementControl
            - Includes modularization into, e.g. data manager / saver
        - Live plotting
        - Dataset converter (possibly two-way)
        - Updating analysis

## Requested discussions:

- (Requested by Adriaan) Suppress warning in load_settings_onto_instrument when trying to set parameters to None (quantify-core#232).
    - When loading parameters from snapshots, sometimes parameters are not yet configured and are set to None. When reloading settings, we get warning due to setting to None.
    - We agree that these warnings should be suppressed.
    - Actionable task? [Adam already working on an MR]


- (Requested by Victor) Heads up: we are not following proper process for milestones/issue triage/features implementation/etc.
    - Problem A:
        - (Victor) We are not tackling older bugs and the backlog is growing.

    - Problem B:
        - (Victor and Adriaan) Accountability for what we have committed to is not being enforced.
        - (Kelvin) Milestones could serve for this.
        - (Adriaan) Milestones can help prioritize, but we are not keeping ourselves accountable, since no one is looking at the Kanban board anymore.

    - Decision:
        - Adriaan and Victor will follow up with sorting out the tasks for dataset v2.
        - We figure out the process-related issues as we go.

## AOB (max 5 mins):

- May be better for core and scheduler to be one repo
    - Easier to merge, deal with dependencies
    - Alternatively, reduce coupling between core and scheduler
    - Not urgent: Postponed for later discussion (after quantify-core v1)

- (Requested by Slava) rename/restructure branches
    - Suggest combining master and develop into main
    - Maintenance branches as well
    - Decision approved, create issue to follow up later

- (Suggested by Adriaan) nightly releases
    - Nightly pypi pre-releases.
    - Put on hold.

- (Requested by Slava) use our own CI fleet (custom runners)
    - More time needed.
    - Great idea no bandwidth to implement it now.
    - To be addressed at later point in time.

- (Requested by Victor) pending implementation: ship tests with pypi
    - Kelvin did you pick this up a while ago?
        - Yes, to be done.

- Always introduce new folks in the MM.

## Pending topics (for next MMs):

- Next meetings allocate time for external contributors/participants since the meeting is public now.
