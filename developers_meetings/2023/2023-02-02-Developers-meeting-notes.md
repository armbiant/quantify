# Epics (APSMM23 release)

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Edgar)
  - https://gitlab.com/quantify-os/quantify-scheduler/-/issues/332 Update CompilationConfig with high-level structure
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Daniel)
- **Parallel work**:
  1. [milestone 3](https://gitlab.com/groups/quantify-os/-/milestones/3)  Quantify Platform release aka The Website :tm: (remaining: website + RTD replacement)
  1. Remove deprecated code / usage
  1. Clean up open tickets (~~256~~ **259** open and counting [board](https://gitlab.com/groups/quantify-os/-/boards))
  1. Fast feedback in Quantify (first MRs) (Qblox)
  1. https://gitlab.com/quantify-os/quantify-scheduler/-/issues/320 ProfiledScheduleGettable needs refactoring (Qblox)
  1. https://gitlab.com/quantify-os/quantify-scheduler/-/compare/main...QAE-472-Draft-parallel-hardware-prepare-support (Qblox)
  1. ...

## Highlights

`quantify-core-0.7.0` and `quantify-scheduler-0.11.0` will be released soon.
Packaging has been slightly changed: dependencies will be configured with `pyproject.toml` file.
In order to install packages in developer mode, you will need to change from `pip install -r requirements_dev.txt -e .` to `pip install -e ".[dev]"`

### Breaking changes

- Qblox backend - Replace `"input_gain<n>"` by `"input_gain_<n>"` and `"input_att"` is the property of `"complex_input"` (quantify-scheduler!585)

## Quantify-scheduler

### Merged MRs (highlights)

- Qblox backend - Replace `"input_gain<n>"` by `"input_gain_<n>"` and `"input_att"` is the property of `"complex_input"` (quantify-scheduler!585)
- Acquisition - Raise an exception if user tries to use same acquisition index and channel for different operations, and only extract data from used modules (quantify-scheduler!573)

## Quantify-core

### Merged MRs (highlights)

- Installation - Instead of `requirements.txt` and `requirements_dev.txt` `quantify-core` uses optional requirements. Use `pip install quantify-core[dev]` to install all of them. (quantify-core!386)

## Requested discussions / Any other business

### @slavoutich: Minimum required `qcodes` version

> MR quantify-core!421 requires bumping minimal requirement of qcodes to `qcodes>=0.37`.
> Is it safe to do or we better postpone this for compatibility reasons?

Due to pyqtgraph fixed in qcodes 0.37  
Will bumping break end user code?  
Edgar: Let's just try to bump

### @rsokolewicz: Created 3 quantify-core documentation issues

**_Up for grabs, would be great if someone would take on one or more of these!_**

- https://gitlab.com/quantify-os/quantify-core/-/issues/356
- https://gitlab.com/quantify-os/quantify-core/-/issues/355
- https://gitlab.com/quantify-os/quantify-core/-/issues/354



## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

*(empty)*

