## Epics

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Tobias)
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Robert, Edgar, Daniel)
  - We aim to deliver a "connecting sched & core" "what is possible with Quantify" showcase depicting T1 experiment on Qblox hardware :hourglass:
    - Next to that, we are adding Qblox Quantify showcases for transmon, spin, NV centers to [Qblox RTD](https://qblox-qblox-instruments.readthedocs-hosted.com/en/master/) :hourglass:
  - _We are going over the existing tutorials of quantify-scheduler and bringing them up-to-date/removing depr code_ :white_check_mark:
- **Parallel work**:
  1. _[milestone 3](https://gitlab.com/groups/quantify-os/-/milestones/3)  Quantify Platform release aka The Website :tm: (remaining: website + RTD polish)_ :white_check_mark:
  1. Clean up open tickets (~~235~~ **236** open [board](https://gitlab.com/groups/quantify-os/-/boards)) :hourglass:


## Highlights

ZI LabOne backend has bin fixed in `main` (quantfiy-scheduler!623).

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Schedules - Add `nv_dark_esr_sched_nco` spectroscopy schedule using SetClockFrequency Operation to sweep the NCO frequency (quantify-scheduler!639)

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Requested discussions / Any other business

*(empty)*

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

*(empty)*

