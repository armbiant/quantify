## Epics (APSMM23 release)

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Edgar)
  - https://gitlab.com/quantify-os/quantify-scheduler/-/issues/332 Update CompilationConfig with high-level structure
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Daniel)
- **Parallel work**:
  1. [milestone 3](https://gitlab.com/groups/quantify-os/-/milestones/3)  Quantify Platform release aka The Website :tm: (remaining: website + RTD replacement)
  1. Remove deprecated code / usage
  1. Clean up open tickets (**247** open and counting [board](https://gitlab.com/groups/quantify-os/-/boards))
  1. Fast feedback in Quantify (first MRs) (Qblox)
  1. https://gitlab.com/quantify-os/quantify-scheduler/-/issues/320 ProfiledScheduleGettable needs refactoring (Qblox)
  1. https://gitlab.com/quantify-os/quantify-scheduler/-/compare/main...QAE-472-Draft-parallel-hardware-prepare-support (Qblox)
  1. ...

## Highlights

- We are trying to improve our code quality and consistensy of deprecation workflow. Uncaught `FutureWarning` in Quantify code base will be an error soon (quantify-scheduler!568).

### Breaking changes

*(empty)*

## Quantify-scheduler

### Merged MRs (highlights)

- Waveforms - Fix `sudden_net_zero` waveform generation function misunderstands `amp_B` (quantify-scheduler!549, quantify-scheduler#390)
- Qblox ICCs - Replace `"acq_mapping"` by `"trace_acq_channel"` in the compiled schedule (quantify-scheduler!515)

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Requested discussions / Any other business

### Acquisition index rename (@gdenes)

> I've a proposition, which I think we should do as part of the acquisition redesign.
> We should rename the `acq_index`, because it's very ambiguous from the perspective of Qblox. The acquisition index on qblox-instrument loosely corresponds to `acq_channel` in quantify, and bin on `qblox-instrument` loosely corresponds to `acq_index` in quantify. This is very confusing. I'm fine with label/coordinate/tag/etc., almost anything (not bin, because there's not always a one-to-one correspondance between the hardware bin).
https://gitlab.com/groups/quantify-os/-/epics/5#note_1169469747

- If you have experiment without repetitions -- `acq_index` in quantify is called bin in qblox-instruments and `acq_channel` in quantify is called acquisition index in qblox-instruments
- Historical context: at abstract level, channels == "different things", index == indicating the iteration/measure, that measures certain channels
- We use the same names on different abstraction layers (occuts in both Qblox and ZI backends), this should be avoided
- So, either `acq_index` in quantify or acquisition index in qblox-instruments should be renamed
- Gabor will discuss this Qblox internally to determine what name changes are considered acceptable (i.e. option to change in qblox-instruments?)

### Using objects in SimpleNodeConfig.compilation_func instead of str (@tmiddelburg)

> Suggestion to sort of move `import_python_object_from_string` functionality to higher level, allowing for improving code clarity
> See https://gitlab.com/quantify-os/quantify-scheduler/-/issues/393

Yes, this is a great idea and should be implemented.


## Extra discussion

### Qiskit integration

- Qiskit interface is postponed to after APSMM23 due to feasibility reasons
- First thing would be to get some automated tests to Ksenia's code so it does not get desynchronized
  - _Simple smoke tests without necessarily checking the output such that we get full line coverage_
  - https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/465
  - Also see [2022-12-08-Developers-meeting-notes.md](../2022/2022-12-08-Developers-meeting-notes.md#Qiskit-Quantify-Interface)
- Kelvin: Already present some experimental version on APSMM?
- _APSMM24_: allow Qiskit schedule to be executed in Quantify


### Formalizing how hardware parameters are updated inside schedules [organized by @djweigand and/or @damazdejong]

Would be awesome to discuss that on the next developers meeting :)

Daniel: possibly Jan 12th intro to outline intention :-)

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

### Formalizing how hardware parameters are updated inside schedules (aka parametrized schedules) [organized by @djweigand and/or @damazdejong] 