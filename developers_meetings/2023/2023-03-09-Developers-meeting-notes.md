# Epics (APSMM23 release)

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Hiresh/Tobias)
  - Tobias will come with an updated plan on what will be done first and what is aimed to finish before APSMM
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Daniel)
  - We aim to deliver a "connecting sched & core" "what is possible with Quantify" showcase depicting T1 experiment on Qblox hardware
    - Next to that, we are adding Qblox Quantify showcases for transmon, spin, NV centers to [Qblox RTD](https://qblox-qblox-instruments.readthedocs-hosted.com/en/master/)
  - We are going over the existing tutorials of quantify-scheduler and bringing them up-to-date/removing depr code
- **Parallel work**:
  1. [milestone 3](https://gitlab.com/groups/quantify-os/-/milestones/3)  Quantify Platform release aka The Website :tm: (remaining: website + RTD polish)
  1. Clean up open tickets (~~249~~ **247** open and counting [board](https://gitlab.com/groups/quantify-os/-/boards))


## Highlights

We have released an official website! https://quantify-os.org

We had two bugfix releases for `quantify-scheduler` with fixes in Qblox backends, latest version is `0.12.2`.

Zurich Instruments backend is currently broken, as of release `0.12.0`. If you are using ZI, don't upgrade until [this MR](https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/623) is merged.

### Breaking changes

- Qblox backend - Strictly requires v0.9.x of the `qblox-instruments` package (!616)

## Quantify-scheduler

### Merged MRs (highlights)

- Qblox backend - raise DriverVersionError when importing qblox module with incompatible qblox-instruments version (!620)
- Qblox backend - Fix imaginary and real part of the acquisition by swapping them, and fix trigger count formatting (!619)
- Documentation - Replace deprecated code in the Operations and Qubits tutorial (!602)

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Requested discussions / Any other business

*(empty)*

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

*(empty)*

