# Epics (APSMM23 release)

- https://gitlab.com/groups/quantify-os/-/epics/5 **Data handling and acquisition flow** (spearheaded by: Slava, Gabor)
- https://gitlab.com/groups/quantify-os/-/epics/1 **Formalize hardware configuration** (spearheaded by: Hiresh/Tobias)
  - Tobias will come with an updated plan on what will be done first and what is aimed to finish before APSMM
- https://gitlab.com/groups/quantify-os/-/epics/2 **Documentation, User Guide and add _New Tutorials_** (spearheaded by: Daniel)
  - We aim to deliver a "connecting sched & core" "what is possible with Quantify" showcase depicting T1 experiment on Qblox hardware
    - Next to that, we are adding Qblox Quantify showcases for transmon, spin, NV centers to [Qblox RTD](https://qblox-qblox-instruments.readthedocs-hosted.com/en/master/)
  - We are going over the existing tutorials of quantify-scheduler and bringing them up-to-date/removing depr code 
- **Parallel work**:
  1. [milestone 3](https://gitlab.com/groups/quantify-os/-/milestones/3)  Quantify Platform release aka The Website :tm: (remaining: website + RTD polish)
  1. Clean up open tickets (~~256~~ **260** open and counting [board](https://gitlab.com/groups/quantify-os/-/boards))


## Highlights

`quantify-core-0.7.0` and `quantify-scheduler-0.11.1` have been released.

### Breaking changes

- Qblox backend - `"input_att"` can be the property of `"complex_input_0"` and `"complex_output_0"`, but not both at the same time for QRM-RF (quantify-scheduler!596, quantify-scheduler!597)

## Quantify-scheduler

### Merged MRs (highlights)

- Visualization - Make box separation in circuit_diagram_matplotlib always equal to one (quantify-scheduler!589)
- Compilation - Add `determine_relative_latencies` that determines latencies for all port-clock combinations in the hardware config relative to the minimum latency (quantify-scheduler!566, quantify-scheduler#379)
- Qblox backend - `"input_att"` can be the property of `"complex_input_0"` and `"complex_output_0"`, but not both at the same time for QRM-RF (quantify-scheduler!596, quantify-scheduler!597)

## Quantify-core

### Merged MRs (highlights)

*(empty)*

## Requested discussions / Any other business

### State of the epic 5 (Data handling)

@slavoutich: Current state of the data handling is following:
- `quantify-scheduler` has [(almost) implemented](https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests/550) internal machinery to return datasets out of `retrieve_acquisition()`.
- `quantify-core` has [(almost) implemented](https://gitlab.com/quantify-os/quantify-core/-/merge_requests/408) machinery to receive structured data from gettables.

Next step will be actually implementing new-style `ScheduleGettable` in `quantify-core` that returns `xarray` data instead of list of arrays (current behavior).

The problem with that is that it will lead to the change of the dataset structrue and therefore we'll need to gradualy port over lots of analysis, ensure that plotmon will still work, etc.
That will take a lot of time.

### Topic of Trace and unknown clock error

After error for unknown clock was added, previously working code would now fail claiming unknown clock 

```python
sched.add(Trace(duration=16e-6, port="measure", clock="measure_clock",acq_channel=5, t0=0), rel_time = 0, ref_op=reset)
```
After long deliberation, came to the conclusion that error informing the user to add clock to device config or sched is indeed the right approach here

We do need to add to breaking changes in the changelog of 0.11.0, it's not there yet ([changelog](https://quantify-quantify-scheduler.readthedocs-hosted.com/en/main/changelog.html)) (@ereehuis)

Create a ticket for editing the docstring of the Trace operation explaining better how to use it (@rsokolewicz)
- quantify-scheduler#402

## Review open (external) MRs

Given the somewhat decreased number of open MRs, we should be able to review using:
- https://gitlab.com/quantify-os/quantify-scheduler/-/merge_requests
- https://gitlab.com/quantify-os/quantify-core/-/merge_requests

## Pending topics (for next DMs)

*(empty)*

