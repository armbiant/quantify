# Developer's Meetings

In this directory we log the developer's meeting notes, documenting decisions for the Quantify project.

The board of the Quantify Consortium consists of:
- Jules van Oven, CTO Qblox
- Adriaan Rol, Director of R&D, Orange Quantum Systems

Current product owners of Quantify are:
- Jules van Oven
- Adriaan Rol

Current maintainers of Quantify are:
- Kelvin Loh
- Viacheslav Ostroukh
- Edgar Reehuis
- Robert Sokolewicz
